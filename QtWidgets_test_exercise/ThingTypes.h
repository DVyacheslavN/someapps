#ifndef THINGTYPES
#define THINGTYPES

#include <QMetaEnum>

namespace Objects {

const int DROP_CELL_ID = 155;
const char* const THING_MIME_TYPE = "application/castomType";

class ThingTypes
{
    Q_GADGET
public:
    enum FrutTypes
    {
        UnknownType = 100,
        Apple,
        Colla,
        END_OF_FRUT_TYPES
    };

    Q_ENUM(FrutTypes)

    static QString frutNameByType(int type)
    {
        static auto frutNames = QMetaEnum::fromType<ThingTypes::FrutTypes>();
        return frutNames.valueToKey(type);
    }
};
}
#endif // THINGTYPES
