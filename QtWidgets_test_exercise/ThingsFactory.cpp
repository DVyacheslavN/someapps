#include "ThingsFactory.h"
#include "Thing.h"
#include "Resources.h"

#include <QDebug>

using namespace Objects;

ThingsFactory *ThingsFactory::instance()
{
    static ThingsFactory factory;
    return &factory;
}

ThingsFactory::ThingsFactory(QObject *parent) : QObject(parent)
{

}

Thing *ThingsFactory::createThing(int thingType)
{
    if (thingType <= ThingTypes::UnknownType || thingType >= ThingTypes::END_OF_FRUT_TYPES)
    {
        qDebug() << "Incorrect thingType:" << thingType;
        return nullptr;
    }

    QString frutName = ThingTypes::frutNameByType(thingType);
    QString thingImagePath = Resources::instance()->findThingImagePath(frutName);
    if (thingImagePath.isEmpty())
    {
        qDebug() << "Resource hasn't image for the frut:"
                 << ThingTypes::frutNameByType(thingType);
        return nullptr;
    }
    qDebug() << "Create thing, type:" << ThingTypes::frutNameByType(thingType)
             << "image path:" << thingImagePath;
    Thing *thing = new Thing(thingType, thingImagePath);
    return thing;
}

