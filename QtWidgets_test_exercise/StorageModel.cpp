#include "StorageModel.h"
#include <QString>
#include <QMimeData>
#include <QDebug>
#include "ThingsFactory.h"


#include <QIcon>

using namespace Objects;

StorageModel::StorageModel(int row, int colunms, QObject *parent)
    : QAbstractTableModel(parent)
    , m_row(row)
    , m_column(colunms)
{

    int allocatedSize = row * colunms;

    for (int i = 0; i < allocatedSize; i++)
    {
        m_data << QPair<int, Thing*>(0, nullptr);
    }
}

int StorageModel::rowCount(const QModelIndex &) const
{
    return  m_row;
}

int StorageModel::columnCount(const QModelIndex &) const
{
    return m_column;
}

QVariant StorageModel::data(const QModelIndex &index, int role) const
{

    if (!isIndexValid(index))
    {
        return QVariant();
    }

    int listIndex = listIndexFromModelIndex(index);

    int count = m_data[listIndex].first;
    if (count == 0)
    {
        return QVariant();
    }

    Thing* thing = m_data[listIndex].second;
    if (thing == nullptr)
    {
        qWarning() << "Thing is null";
        return QVariant();
    }

    if (role == Qt::DisplayRole)
    {
        return QString::number(count);
    }

    if (role == Qt::DecorationRole)
    {
        return m_cachedPixmap.value(thing->type());
    }

    return QVariant();
}

Qt::ItemFlags StorageModel::flags(const QModelIndex &index) const
{
    if (isIndexValid(index))
    {
        int index_ = listIndexFromModelIndex(index);
        int count = m_data[index_].first;
        if (count == 0)
        {
            return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;
        }

        return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled ;
    }
    return Qt::NoItemFlags;
}

bool StorageModel::dropMimeData(const QMimeData *data, Qt::DropAction action,
                              int, int, const QModelIndex &parent)
{
    if (data->hasFormat("application/x-qabstractitemmodeldatalist"))
    {
        if (action == Qt::MoveAction)
        {
            int row_, column_;
            QByteArray bute = data->data("application/x-qabstractitemmodeldatalist");
            QDataStream stream(&bute, QIODevice::ReadOnly);
            stream >> row_ >> column_;
            QModelIndex index_ = index(row_, column_);
            return moveThing(index_, parent);
        }
    }
    else if (data->hasFormat(THING_MIME_TYPE))
    {
        QByteArray bute = data->data(THING_MIME_TYPE);
        QDataStream stream(&bute, QIODevice::ReadOnly);
        QMap <int, QVariant> roleMap;
        stream >> roleMap;

        int type = roleMap.value(DROP_CELL_ID, -1).toInt();
        if (type != -1)
        {
            Thing * thing = ThingsFactory::instance()->createThing(type);
            if (addThing(parent.row(), parent.column(), thing))
            {
                return true;
            }
            delete thing;
            return false;
        }
    }
    return false;
}

QStringList StorageModel::mimeTypes() const
{
    QStringList types = QAbstractTableModel::mimeTypes();
    types << THING_MIME_TYPE;
    return types;
}

QHash<int, QByteArray> StorageModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractTableModel::roleNames();
    return roles;
}

Qt::DropActions StorageModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

QList<std::tuple<int, int, int>> StorageModel::thingsData()
{
    QList<std::tuple<int, int, int>> thingsData_;
    for (int i = 0; i < m_data.size(); i++)
    {
        int &count = m_data[i].first;
        int type = m_data[i].second != nullptr ? m_data[i].second->type() : ThingTypes::UnknownType;
        thingsData_ << std::make_tuple(i, count, type);
    }
    return thingsData_;
}

/// @param QList <std::Tuple <id(int),  count(int), type(int)>>
void StorageModel::setTingData(QList<std::tuple<int, int, int> > &thingData)
{

    for (int i = 0; i < rowCount(); i++)
    {
        for (int j = 0;j < columnCount(); j++)
        {
            clearCell(index(i, j));
        }
    }

    for (auto &field : thingData)
    {
        int &id  =  std::get<0>(field);

        if (id < 0 || id >= m_data.count())
        {
            qWarning() << "index out of model size, index:" << id;
            emit dataChanged(index(0,0), index(rowCount()-1, columnCount() -1));
            return;
        }
        m_data[id].first = std::get<1>(field);
        Thing *thing = ThingsFactory::instance()->createThing(std::get<2>(field));
        addCachedPixmap(thing);
        m_data[id].second = thing;
        emit dataChanged(index(0,0), index(rowCount()-1, columnCount() -1));
    }
}

bool StorageModel::addThing(int row, int column, Thing *thing)
{
    if (thing == nullptr)
    {
        qWarning() << "Thing is null";
        return false;
    }

    QModelIndex index_ = index(row, column);
    if (!isIndexValid(index_))
    {
        qWarning() << "Incorrect index:" << index_;
        return false;
    }

    int listIndex = listIndexFromModelIndex(index_);
    if (m_data[listIndex].first == 0 && m_data[listIndex].second == nullptr)
    {
        m_data[listIndex].first = 1;
        m_data[listIndex].second = thing;

        addCachedPixmap(thing);
        emit dataChanged(index(row, column), index(row, column));
        return true;
    }
    else if (m_data[listIndex].second != nullptr
             && m_data[listIndex].second->type() == thing->type())
    {
        m_data[listIndex].first++;
        emit dataChanged(index(row, column), index(row, column));
        delete thing;
        return true;
    }

    return false;
}

void StorageModel::removeThing(QModelIndex &index_)
{

    if (!isIndexValid(index_))
    {
        qDebug() << "Index more then size:" << index_;
        return;
    }

    int listIndex = listIndexFromModelIndex(index_);
    if (m_data[listIndex].first > 1)
    {
        m_data[listIndex].first--;
        emit dataChanged(index_, index_);
    }
    else
    {
        clearCell(index_);
    }
}

bool StorageModel::moveThing(const QModelIndex &from, const QModelIndex &to)
{
    if (!isIndexValid(from) || !isIndexValid(to))
    {
        qDebug() << "Index is invalid";
        return false;
    }

    qDebug() << "moveThing";
    int listIndexFrom = listIndexFromModelIndex(from);
    int listIndexTo = listIndexFromModelIndex(to);

    int &countFrom = m_data[listIndexFrom].first;
    Thing *&thingFrom = m_data[listIndexFrom].second;
    if (thingFrom == nullptr || countFrom == 0)
    {
        qDebug() << "Nothing to move count:" << countFrom;
        return false;
    }

    int &countTo = m_data[listIndexTo].first;
    Thing *&thingTo = m_data[listIndexTo].second;
    if (thingTo == nullptr)
    {
        if (countTo != 0)
        {
            qWarning() << "Incorrect count";
            return false;
        }

        countTo = countFrom;
        countFrom = 0;

        thingTo = thingFrom;
        thingFrom = nullptr;

        emit dataChanged(from, to);
    }
    else
    {
        if (countTo == 0)
        {
            qWarning() << "Incorrect count";
            return false;
        }

        if (thingFrom->type() != thingTo->type())
        {
            qDebug() << "Types don't match";
            return false;
        }

        countTo += countFrom;
        countFrom = 0;

        clearCell(from);
        emit dataChanged(from, to);
        return true;
    }
    return false;
}

int StorageModel::listIndexFromModelIndex(const QModelIndex &index) const
{
    return ((index.row() * rowCount()) + index.column());
}

void StorageModel::clearCell(const QModelIndex &index_)
{
    qDebug() << "clearCell";
    if (!isIndexValid(index_))
    {
        qDebug() << "Index is invalid";
        return;
    }

    int listIndex = listIndexFromModelIndex(index_);
    if (m_data[listIndex].first > 0)
    {
        m_data[listIndex].first = 0;
    }

    if (m_data[listIndex].second != nullptr)
    {
        delete m_data[listIndex].second;
        m_data[listIndex].second = nullptr;
    }
    emit dataChanged(index_, index_);
}

void StorageModel::addCachedPixmap(Thing *thing)
{
    if (thing == nullptr || !m_cachedPixmap.value(thing->type()).isNull() )
    {
        qDebug() << "Thing is null or Pixmap exists";
        return;
    }

    m_cachedPixmap[thing->type()] = QIcon(thing->path()).pixmap(120, 150);
}

bool StorageModel::isIndexValid(const QModelIndex &index) const
{
    if (index.row() >= rowCount()
            || index.column() >= columnCount()
            || index.row() < 0
            || index.column() < 0)
    {
        qWarning() << "index out of row or column size, index:" << index;
        return false;
    }
    int listIndex = listIndexFromModelIndex(index);
    if (listIndex < 0 || listIndex >= m_data.count())
    {
        qWarning() << "index out of model size, index:" << index;
        return false;
    }
    return true;
}

