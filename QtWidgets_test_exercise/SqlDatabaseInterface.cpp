/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#include "SqlDatabaseInterface.h"

#include <QDebug>

#include <tuple>

namespace
{
const QString splitSign = ", ";
const QString endSign = " ); ";
const QString closeGroupSign = " ) ";
const QString startSign = " ( ";
}

SqlDatabaseInterface::SqlDatabaseInterface(QSqlDatabase &sqlDatabase, QSqlQuery *query, QObject *parent)
    : QObject (parent)
    , m_sqlDatabase(sqlDatabase)
    , m_query(query)
{
}

bool SqlDatabaseInterface::createDatabase(const QString databaseName)
{
    m_sqlDatabase.setDatabaseName(databaseName);
    if (!m_sqlDatabase.open())
    {
        qDebug() << "Database wasn't created";
        return false;
    }
    return true;
}

QString SqlDatabaseInterface::prepareCreateTableQuery(const QList<QPair<QString, QString>> &tableDiscription)
{
    int columnCount = tableDiscription.size();
    if (columnCount == 0)
    {
        qDebug() << "Nothing to prepare";
        return QString();
    }

    QString query(::startSign);

    auto beginIterator = tableDiscription.begin();
    auto endIterator = tableDiscription.end();
    auto penultimateIterator = columnCount > 0 ? endIterator - 1 : endIterator;

    for (;beginIterator < penultimateIterator; beginIterator++)
    {
        const QString &columnName = beginIterator->first;
        const QString &columnType = beginIterator->second;
        query.append(columnName +" " + columnType + ::splitSign);
    }

    query.append(penultimateIterator->first + " " + penultimateIterator->second).append(::endSign);

    return query;
}

QStringList SqlDatabaseInterface::prepareUpdateTableQueries(QList<std::tuple<int, QString, QString> > &updateDataTableDiscription)
{   
    QStringList queries;
    const QString query = " SET ";
    for (auto & updateDiscriptionField : updateDataTableDiscription)
    {
        QString buff;
        buff = query;
        buff.append(std::get<1>(updateDiscriptionField))
                .append(" = ")
                .append(std::get<2>(updateDiscriptionField))
                .append(" WHERE ")
                .append("_id")
                .append(" = ")
                .append(QString::number(std::get<0>(updateDiscriptionField)));
        queries << buff;
    }
    return queries;
}

bool SqlDatabaseInterface::updateRequestByIDs(const QString &nameTable, QList<std::tuple<int, QString, QString>> &updateDataTableDiscription)
{
    const QString updateQuery = "UPDATE " + nameTable;
    QStringList preparedUpdateQueries = prepareUpdateTableQueries(updateDataTableDiscription);
    for (const auto &updateField : preparedUpdateQueries)
    {
        QString buff;
        buff = updateQuery;
        buff.append(updateField);
        qDebug() << __PRETTY_FUNCTION__ << "request:" << buff;
        m_query->prepare(buff);
        if (!m_query->exec())
        {
            return false;
        }
    }
    return true;
}

int SqlDatabaseInterface::rowsCount(const QString &tableName)
{
    m_query->exec("SELECT COUNT(*) FROM " + tableName);
    if (m_query->next()) {
        return m_query->value(0).toInt();
    }
    return 0;
}

bool SqlDatabaseInterface::insertDataRow(const QString &nameTable, QList<QPair<QString, QString>> &dataRowDiscription)
{
    int columnCount = dataRowDiscription.size();
    if (columnCount == 0)
    {
        qDebug() << "Nothing to insert, column count:" << columnCount;
        return false;
    }

    QString columns(::startSign);
    QString values("VALUES" + ::startSign);
    QString insertQuery("INSERT INTO " + nameTable);

    auto beginIterator = dataRowDiscription.begin();
    auto endIterator = dataRowDiscription.end();
    auto penultimateIterator = columnCount > 0 ? endIterator - 1 : endIterator;

    for (;beginIterator < penultimateIterator; beginIterator++)
    {
        QString &columnName = beginIterator->first;
        QString &value = beginIterator->second;

        columns.append(columnName).append(::splitSign);
        values.append(value).append(::splitSign);
    }

    columns.append(penultimateIterator->first).append(::closeGroupSign);
    values.append(penultimateIterator->second).append(::endSign);

    insertQuery.append(columns).append(values);

    qDebug() << __PRETTY_FUNCTION__ << insertQuery;
    if (m_query->exec(insertQuery))
    {
        return true;
    }

    return false;
}

void SqlDatabaseInterface::insertDataRows(const QString &nameTable, QList<QList<QPair<QString, QString> > > &dataRowDiscription)
{
    if (dataRowDiscription.size() == 0)
    {
        return;
    }

    for (auto &row : dataRowDiscription)
    {
        insertDataRow(nameTable, row);
    }
}

QList<QList<QVariant>> SqlDatabaseInterface::getTable(const QString &tableName)
{
    QList<QList<QVariant>> table;
    m_query->prepare("SELECT * FROM " + tableName);
    if (!m_query->exec())
    {
        qWarning() << "Table wasn't created";
        return table;
    }

    while (m_query->next() != false)
    {
        QList<QVariant> m_rowList;
        for (int i = 0; m_query->value(i).isValid(); i++)
        {
            m_rowList << m_query->value(i);
        }
        table << m_rowList;
    }
    return table;
}

bool SqlDatabaseInterface::createTable(const QString &tableName,const QList<QPair<QString, QString>> &tableDiscription)
{
    m_query->prepare("SELECT * FROM " + tableName);
    if (!m_query->exec())
    {
        QString preparedQueryCreateTable = prepareCreateTableQuery(tableDiscription);
        QString createQuery("CREATE TABLE " + tableName + " " + preparedQueryCreateTable);
        qDebug() << __PRETTY_FUNCTION__ << "createQuery:" << createQuery;
        m_query->exec(createQuery);
        m_query->prepare("SELECT * FROM " + tableName);
        if (!m_query->exec())
        {
            qWarning() << "Table wasn't created";
            return false;
        }
    }
    qDebug() << "Table with this name exist, table name:" << tableName;
    return true;
}
