#-------------------------------------------------
#
# Project created by QtCreator 2019-12-09T16:18:23
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AppleApp
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    Thing.cpp \
    ThingsFactory.cpp \
    Resources.cpp \
    ThingWidget.cpp \
    StorageWidget.cpp \
    StorageModel.cpp \
    RootController.cpp \
    SqlDatabaseInterface.cpp

HEADERS  += mainwindow.h \
    Thing.h \
    ThingsFactory.h \
    ThingTypes.h \
    Resources.h \
    ThingWidget.h \
    StorageWidget.h \
    StorageModel.h \
    RootController.h \
    SqlDatabaseInterface.h

FORMS    += mainwindow.ui

DISTFILES += \
    resources/resource.json

copydata.commands = $(COPY_DIR) $$PWD/resources $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
