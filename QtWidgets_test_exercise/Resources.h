#ifndef RESOURCES_H
#define RESOURCES_H

#include <QObject>
#include <QMap>

class Resources : public QObject
{
    Q_OBJECT

public:
    static Resources *instance();

    QString findThingImagePath(QString &thingName);

private:
    Resources(QObject *parent = 0);
    void loadResources(QJsonDocument &resourcesJson);

    // name and path
    QMap<QString, QString> m_resouces;
};

#endif // RESOURCES_H
