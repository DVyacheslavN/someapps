#ifndef THINGFACTORY_H
#define THINGFACTORY_H

#include <QObject>

#include <ThingTypes.h>

class Thing;

class ThingsFactory : public QObject
{
    Q_OBJECT
public:
    static ThingsFactory *instance();
    Thing *createThing(int thingType);
private:
    explicit ThingsFactory(QObject *parent = 0);

signals:

public slots:
};

#endif // THINGFACTORY_H
