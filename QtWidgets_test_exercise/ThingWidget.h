#ifndef THING_WIDGET_H
#define THING_WIDGET_H
#include <QLabel>
#include <QWidget>

#include <Thing.h>

class ThingWidget : public QLabel
{
    Q_OBJECT
public:
    explicit ThingWidget(Thing *thing, QWidget *parent = nullptr);

    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void setThing(Thing *thing);
    Thing *thing();

signals:
    void rightMouseButtionClicked();

private:
    void updateDropBuffer();

    QString typePicture;
    QPoint dragStart;
    Thing *m_thing;
    QByteArray m_dropBuffer;
};

#endif // THING_WIDGET_H
