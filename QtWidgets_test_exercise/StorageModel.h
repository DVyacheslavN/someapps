#ifndef STOREMODEL_H
#define STOREMODEL_H
#include <QAbstractTableModel>
#include <QObject>
#include <QVariant>
#include <QHash>
#include <QList>
#include <QModelIndex>
#include <QMap>
#include <QQueue>
#include <QPair>

#include <Thing.h>
#include <utility>

#include <RootController.h>
#include <QPixmap>

class StorageModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    StorageModel(int row,int colunms, QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                      int row, int column, const QModelIndex &parent) override;
    QStringList mimeTypes() const override;
    Qt::DropActions supportedDropActions() const override;

    /// @brief
    /// @return QList <std::Tuple <id(int),  count(int), type(int)>>
    QList<std::tuple<int, int, int>> thingsData();
    /// @brief
    /// @param QList <std::Tuple <id(int),  count(int), type(int)>>
    void setTingData(QList<std::tuple<int, int, int> > &thingData);
    bool addThing(int row, int column, Thing *thing);
    void removeThing(QModelIndex &index_);
    bool moveThing(const QModelIndex &from, const QModelIndex &to);

private:
    int listIndexFromModelIndex(const QModelIndex &index) const;
    void clearCell(const QModelIndex &index_);
    void addCachedPixmap(Thing *thing);
    bool isIndexValid(const QModelIndex &index) const;

    QList <QPair<int, Thing*>> m_data;
    QHash <int, QPixmap> m_cachedPixmap;
    int m_row;
    int m_column;
    int m_cachedCount;
};

#endif // STOREMODEL_H
