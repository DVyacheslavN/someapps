#include "RootController.h"

#include "StorageModel.h"
#include "ThingWidget.h"
#include "ThingTypes.h"
#include "ThingsFactory.h"
#include "SqlDatabaseInterface.h"
#include "StorageWidget.h"

#include <QMetaEnum>
#include <QDebug>

using namespace Objects;

namespace
{
    const QString TableName("ThingsTable");
    //                                              name          type
    const QList<QPair<QString, QString>> Columns { {"_id",      "integer PRIMARY KEY NOT NULL"},
                                                   {"count",    "integer"},
                                                   {"type",     "integer"}};
}

RootController::RootController(StorageModel *storageModel, ThingWidget *thingWidget,
                               SqlDatabaseInterface *sqlDatabaseInterface,
                               StorageWidget *storageWidget,
                               QObject *parent)
    : QObject (parent)
    , m_storageModel(storageModel)
    , m_thingWidget(thingWidget)
    , m_sqlDatabaseInterface(sqlDatabaseInterface)
    , m_storageWidget(storageWidget)
{
    if (m_thingWidget != nullptr)
    {
        connect(m_thingWidget, &ThingWidget::rightMouseButtionClicked,
                this, &RootController::handleRightMouseClickedThingWidget);
    }

    if (m_storageWidget != nullptr)
    {

        connect(m_storageWidget, &StorageWidget::rightMouseButtionClicked,
                this, &RootController::handleRightMouseButtionClickedStorageWidget);
    }

    createThingsTable();
    initializeTable();
}

void RootController::handleRightMouseButtionClickedStorageWidget(QModelIndex clickedIndex)
{
    if (m_storageModel != nullptr)
    {
        m_storageModel->removeThing(clickedIndex);
        m_storageModel->thingsData();
    }
}

void RootController::handleRightMouseClickedThingWidget()
{
    if (m_thingWidget->thing() != nullptr)
    {
        Thing *currentThing = m_thingWidget->thing();
        int thingType = currentThing->type();
        Thing *nextThing = ThingsFactory::instance()->createThing(nextFruit(thingType));
        if (nextThing != nullptr)
        {
            m_thingWidget->setThing(nextThing);
            delete currentThing;
        }
    }
}

void RootController::saveModelToDataBase()
{
    if (m_storageModel == nullptr || m_sqlDatabaseInterface == nullptr)
    {
        return;
    }

    QList<std::tuple<int, int, int>> thingsData = m_storageModel->thingsData();

    QList<std::tuple<int, QString, QString>> data;

    for (auto &field : thingsData)
    {
        data << std::make_tuple(std::get<0>(field), QString("count"), QString::number(std::get<1>(field)));
        data << std::make_tuple(std::get<0>(field), QString("type"), QString::number(std::get<2>(field)));
    }

    m_sqlDatabaseInterface->updateRequestByIDs(::TableName, data);
}

void RootController::loadModelFromDataBase()
{
    if (m_sqlDatabaseInterface == nullptr || m_storageModel == nullptr)
    {
        qDebug() << "SqlDatabaseInterface or storageModel is null";
        return;
    }

    QList<QList<QVariant>> model = m_sqlDatabaseInterface->getTable(::TableName);

    QList<std::tuple<int, int, int>> data;
    for (auto &field : model)
    {
        data.append(std::make_tuple(field.at(0).toInt(), field.at(1).toInt(), field.at(2).toInt()));
    }

    m_storageModel->setTingData(data);
}

void RootController::handleStartReques()
{
    if (m_storageModel == nullptr || m_thingWidget == nullptr || m_storageWidget == nullptr)
    {
        qDebug() << "Some of Objects is null";
        return;
    }

    m_storageWidget->setDisabled(false);
    m_thingWidget->setDisabled(false);
    emit gameStarted();
}

void RootController::handleRequestStopGame()
{
    if (m_thingWidget == nullptr || m_storageWidget == nullptr)
    {
        qDebug() << "Some of Objects is null";
        return;
    }

    m_storageWidget->setDisabled(true);
    m_thingWidget->setDisabled(true);

    saveModelToDataBase();
    emit gameStopped();
}

void RootController::createThingsTable()
{
    if (m_sqlDatabaseInterface == nullptr)
    {
        qDebug() << "SQL Database Interface is null";
        return;
    }

    if (m_sqlDatabaseInterface->createDatabase("source.db"))
    {

        m_sqlDatabaseInterface->createTable(::TableName, ::Columns);
    }
    else
    {
        qDebug() << "Database wasn't opend";
    }
}

void RootController::initializeTable()
{
    if (m_storageModel == nullptr)
    {
        qDebug() << __PRETTY_FUNCTION__ << "StorageModle is null";
        return;
    }
    if (m_sqlDatabaseInterface == nullptr)
    {
        qDebug() << __PRETTY_FUNCTION__ << "SqlDatabaseInterface is null";
        return;
    }

    const int modelSize = m_storageModel->rowCount() * m_storageModel->columnCount();
    if (m_sqlDatabaseInterface->rowsCount(::TableName) == modelSize)
    {
        qDebug() << "Table has already been initialized";
        return;
    }


    QList<DbRow> tableRows;
    for (int i = 0; i < modelSize; i++)
    {
        DbRow row = {};
        for (int j = 0; j < m_storageModel->columnCount(); j++)
        {
            row.append(DbCell(Columns.at(j).first, j == 0 ? QString::number(i) : "0"));
        }
        tableRows.append(row);
    }

    m_sqlDatabaseInterface->insertDataRows(::TableName, tableRows);
    qDebug() << "Count" << m_sqlDatabaseInterface->rowsCount(::TableName);
}

int RootController::nextFruit(int currentFruit)
{
    if (currentFruit <= ThingTypes::UnknownType || currentFruit >= ThingTypes::END_OF_FRUT_TYPES)
    {
        qDebug() << "Incorrect type:" << currentFruit;
        return ThingTypes::UnknownType;
    }

    if (++currentFruit == ThingTypes::END_OF_FRUT_TYPES)
    {
        return ThingTypes::UnknownType + 1;
    }
    return currentFruit;
}
