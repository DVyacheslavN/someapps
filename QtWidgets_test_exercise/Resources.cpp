#include "Resources.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

namespace
{
   const QString ResourceFile = "resources/resource.json";
}

Resources *Resources::instance()
{
    static Resources resources;
    return &resources;
}

Resources::Resources(QObject *parent) : QObject(parent)
{
    QFile file(ResourceFile);
    if (file.open(QFile::ReadOnly))
    {
        QJsonDocument resourcesJson = QJsonDocument::fromJson(file.readAll());
        loadResources(resourcesJson);
    }
    else
    {
        qWarning() << QString("File %1 hasn't been opened").arg(ResourceFile);
    }
}

void Resources::loadResources(QJsonDocument &resourcesJson)
{
    if (resourcesJson.isEmpty() || !resourcesJson.isArray() || !resourcesJson.array().size())
    {
        qWarning() << "Incorrect Json file, file:" << resourcesJson;
        return;
    }

    int size = resourcesJson.array().size();
    for(int i = 0; i < size; i++)
    {
        // below must be more checkings, but i haven't wanted to spend time for it :)
        QJsonObject frut = resourcesJson.array().at(i).toObject();
        m_resouces[frut.value("name").toString()] = frut.value("path").toString();
    }
}

QString Resources::findThingImagePath(QString &thingName)
{
    if (!m_resouces.size())
    {
        qWarning() << "Resources hasn't been loaded";
        return QString("");
    }
    return m_resouces.value(thingName, QString(""));
}

