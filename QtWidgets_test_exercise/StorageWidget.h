#ifndef STORAGE_WIDGET_H
#define STORAGE_WIDGET_H

#include <QObject>
#include <QTableView>

#include <QMimeData>

class StorageWidget : public QTableView
{
    Q_OBJECT

public:
    StorageWidget(QAbstractItemModel* model, QWidget *parent = nullptr);

    void mouseReleaseEvent(QMouseEvent *event) override;
    void setModel(QAbstractItemModel *model) override;
    void dropEvent(QDropEvent *event) override;
    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) override;

signals:
    void rightMouseButtionClicked(QModelIndex position);

};

#endif // STORAGE_WIDGET_H
