#ifndef THING_H
#define THING_H

#include <QObject>

class ThingsFactory;
class Thing : public QObject
{
    Q_OBJECT
    friend ThingsFactory;
public:

    QString path();
    int type();

    virtual ~Thing();
private:
    Thing(int type, QString imageUrl, QObject *parent = nullptr);

private:
    QString m_path;
    int m_type;
};

#endif // THING_H
