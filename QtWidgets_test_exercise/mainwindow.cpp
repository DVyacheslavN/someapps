#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Thing.h"
#include "ThingWidget.h"
#include "StorageWidget.h"
#include "StorageModel.h"
#include "RootController.h"
#include "SqlDatabaseInterface.h"

#include <QtDebug>

using namespace Objects;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_thingsFactory(ThingsFactory::instance()),
    db(QSqlDatabase::addDatabase("QSQLITE"))
{
    QSqlQuery *query = new QSqlQuery;
    auto sqlDbIface = new SqlDatabaseInterface(db, query, this);

    auto storageModel = new StorageModel(3, 3, this);
    auto thingWidget = new ThingWidget(m_thingsFactory->createThing(ThingTypes::Colla), this);
    auto storageWidget = new StorageWidget(storageModel, this);
    auto rootController = new RootController(storageModel, thingWidget, sqlDbIface, storageWidget, this);

    ui->setupUi(this);
    ui->ThingLayout->addWidget(thingWidget);
    ui->StorageLayout->addWidget(storageWidget);

    connect(ui->StartGame, &QPushButton::clicked, rootController, &RootController::handleStartReques);
    connect(ui->Exit, &QPushButton::clicked, rootController, &RootController::handleRequestStopGame);
    connect(ui->LoadSavedGame, &QPushButton::clicked, rootController, &RootController::loadModelFromDataBase);
}

MainWindow::~MainWindow()
{
    delete ui;
}

