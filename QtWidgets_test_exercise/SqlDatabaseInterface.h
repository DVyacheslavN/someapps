/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#ifndef SQL_DB_IFACE
#define SQL_DB_IFACE

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>
#include <QVariant>

typedef QPair<QString, QString> DbCell;
typedef QList<DbCell> DbRow;

class SqlDatabaseInterface : public QObject
{
    Q_OBJECT
public:
    enum CreateStatus
    {
        NotCreated = 0,
        Created,
        AlredyExist,
    };

    SqlDatabaseInterface(QSqlDatabase &sqlDatabase, QSqlQuery *query, QObject *parent = nullptr);

    QList<QList<QVariant>> getTable(const QString &tableName);
    bool createTable(const QString &tableName, const QList<QPair<QString, QString> > &tableDiscription);
    bool createDatabase(const QString databaseName);
    QString prepareCreateTableQuery(const QList<QPair<QString, QString> > &tableDiscription);
    QString updateString_(QList<QString> *nameColumns);
    QStringList prepareUpdateTableQueries(QList<std::tuple<int, QString, QString> > &updateDataTableDiscription);

    ///
    /// @brief insertDataTable
    /// @param nameTable
    /// @param dataTableDiscription QPair first is columnName, second is value
    /// @return true if success
    ///
    bool insertDataRow(const QString &nameTable, QList<QPair<QString, QString>> &dataRowDiscription);
    void insertDataRows(const QString &nameTable, QList<QList<QPair<QString, QString>>> &dataRowDiscription);

    /// @brief
    /// @param nameTable is name of table
    /// @param updateDataTableDiscription QList<std::tuple<id(int), column(QString), value(QString)> >
    bool updateRequestByIDs(const QString &nameTable, QList<std::tuple<int, QString, QString> > &updateDataTableDiscription);
    int rowsCount(const QString &tableName);

private:
    QSqlDatabase &m_sqlDatabase;
    QSqlQuery *m_query;
};

#endif // SQL_DB_IFACE
