#include "StorageWidget.h"

#include <QMouseEvent>
#include <QDebug>
#include <QHeaderView>

#include "StorageModel.h"

StorageWidget::StorageWidget(QAbstractItemModel* model, QWidget *parent)
    : QTableView (parent)
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setDragEnabled(true);
    setAcceptDrops(true);
    setSelectionMode(QAbstractItemView::SingleSelection);
    verticalHeader()->hide();
    horizontalHeader()->hide();
    setDefaultDropAction(Qt::MoveAction);
    setDisabled(true);

    if (model != nullptr)
    {
        setMinimumSize(150 * model->rowCount(), 150 * model->columnCount());
        setMaximumSize(150 * model->rowCount(), 150 * model->columnCount());
        setModel(model);
    }
    else
    {
        qDebug() << "Model is null";
    }
}

void StorageWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event == nullptr)
    {
        return;
    }

    if (event->button() == Qt::RightButton)
    {
        emit rightMouseButtionClicked(indexAt(event->pos()));
    }
}

void StorageWidget::dropEvent(QDropEvent *event)
{
    if (event == nullptr)
    {
        qDebug() << "Event is Null";
    }

    QAbstractItemView::dropEvent(event);
}

void StorageWidget::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    QTableView::selectionChanged(selected, deselected);
}

void StorageWidget::setModel(QAbstractItemModel *model)
{
    if (model == nullptr)
    {
        qDebug() << "Model is Null";
    }

    QTableView::setModel(model);
    for (int i = 0; i < model->columnCount(); i++)
    {
        setColumnWidth(i, 150);
        for(int j = 0; j < model->rowCount(); j++)
        {
            setRowHeight(j, 150);
        }
    }
}
