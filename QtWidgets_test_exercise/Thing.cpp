#include "Thing.h"

static int count(0);

#include <QtDebug>
Thing::~Thing()
{
    count--;
    qDebug() << "Delete count:" << count;
}

Thing::Thing(int type, QString imageUrl, QObject *parent)
    : QObject (parent),
      m_path(imageUrl),
      m_type(type)
{
    count++;
    qDebug() << "crate Count:" << count;
}

QString Thing::path()
{
    return m_path;
}

int Thing::type()
{
    return m_type;
}
