#ifndef STORAGECONTROLLER_H
#define STORAGECONTROLLER_H

#include <QObject>
#include <QModelIndex>

class StorageModel;
class ThingWidget;
class SqlDatabaseInterface;
class StorageWidget;

class RootController : public QObject
{
    Q_OBJECT
public:
    RootController(StorageModel *storageModel = nullptr
            , ThingWidget *thingWidget = nullptr
            , SqlDatabaseInterface *sqlDatabaseInterface = nullptr
            , StorageWidget *storageWidget = nullptr
            , QObject *parent = nullptr);

    void handleRightMouseButtionClickedStorageWidget(QModelIndex clickedIndex);
    void handleRightMouseClickedThingWidget();

    void createThingsTable();
    void initializeTable();

    void saveModelToDataBase();

public slots:
    void loadModelFromDataBase();
    void handleStartReques();
    void handleRequestStopGame();

signals:
    void gameStarted();
    void gameStopped();


private:
    int nextFruit(int currentFruit);

    StorageModel * const m_storageModel;
    ThingWidget * const m_thingWidget;
    SqlDatabaseInterface * const m_sqlDatabaseInterface;
    StorageWidget * const m_storageWidget;
};

#endif // STORAGECONTROLLER_H
