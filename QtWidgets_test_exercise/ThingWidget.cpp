#include "ThingWidget.h"

#include <QDebug>

#include <QMouseEvent>
#include <QMimeData>
#include <QPixmap>
#include <QImage>
#include <QDrag>
#include <QApplication>

#include "ThingTypes.h"

using namespace Objects;

ThingWidget::ThingWidget(Thing *thing, QWidget *parent)
    : QLabel(parent),
      m_thing(thing)
{
    setMinimumSize(125, 100);
    setMaximumSize(125, 100);
    setThing(thing);
    setDisabled(true);
}

void ThingWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event == nullptr)
    {
        qDebug() << "Event is null";
        return;
    }

    if (m_thing == nullptr)
    {
        qDebug() << "Thing is null";
        return;
    }

    if ((event->buttons() & Qt::LeftButton)
            && QApplication::startDragDistance() <= (event->pos() - dragStart).manhattanLength())
    {
        QDrag *drag = new QDrag(this);
        QMimeData *mimeData = new QMimeData;
        mimeData->setData(THING_MIME_TYPE, m_dropBuffer);
        drag->setMimeData(mimeData);
        drag->setPixmap(*pixmap());
        drag->exec(Qt::CopyAction);
    }

    event->accept();
}

void ThingWidget::mousePressEvent(QMouseEvent *event)
{
    if (event != nullptr && event->button() == Qt::RightButton)
    {
        emit rightMouseButtionClicked();
    }
}

void ThingWidget::setThing(Thing *thing)
{
    if (thing != nullptr)
    {
        m_thing = thing;
        setPixmap(QPixmap::fromImage(QImage(thing->path()).scaled(this->size())));

        updateDropBuffer();
    }
}

Thing *ThingWidget::thing()
{
    return m_thing;
}

void ThingWidget::updateDropBuffer()
{
    m_dropBuffer.clear();
    QDataStream Steam(&m_dropBuffer, QIODevice::WriteOnly);
    QMap <int, QVariant> roleMap;
    roleMap[DROP_CELL_ID] = m_thing->type();
    Steam << roleMap;
}

