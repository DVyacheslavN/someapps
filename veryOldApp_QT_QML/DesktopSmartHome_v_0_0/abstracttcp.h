#ifndef ABSTRACTTCP_H
#define ABSTRACTTCP_H
#include <QObject>
#include <QTcpSocket>
#include <QByteArray>
#include <desctopprojenums.h>
class AbstractTCP : public QObject
{
    Q_OBJECT
public:
    AbstractTCP();
    bool v_connectStatus=false;
   // QTcpSocket *v_TcpSocket;

     qint16 v_nextBlockSize=0;
public slots:
    virtual void sl_ConnectTo(QString vHost,int port)=0;
private:
    //virtual void pr_selectType()=0;

private slots:
    virtual void pr_connect();
    void sl_Connected();
signals:
    void s_EventTCP(QString,Ep::EventT ev);
    void si_Connected();

    /*inner sig*/
    void si_ConectRedy();



};

#endif // ABSTRACTTCP_H
