#ifndef EVENT_H
#define EVENT_H

#include <QObject>
#include <eventabstract.h>
class Event : public  EventAbstract
{
    Q_OBJECT
public:
    explicit Event();

    virtual void sl_addEvent(QString str, Ep::EventT type);
signals:

public slots:
};

#endif // EVENT_H
