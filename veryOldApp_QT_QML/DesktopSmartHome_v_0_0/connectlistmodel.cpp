#include "connectlistmodel.h"
#include "QDebug"
#include <QMetaEnum>
ConnectListModel::ConnectListModel()
{
    connect(this,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),this,SIGNAL(roleConnectChanged()));
    // m_roleSetFromEnum();
    int col=1;
    int row=5;
    for (int i=0;i<row;i++)
    {
        QHash<int,QVariant> buf;
        buf[ip]=i;
        buf[Alias]="";
        {
            cont_Model.append(buf);
        }
    }
    int g=2323;
    m_AppEndRow(QString("192.168.0.1"),g);
    m_AppEndRow(QString("192.168.0.104"),g);
    m_AppEndRow(QString("127.0.0.1"),g);
    v_timer.setInterval(300);
    connect((&v_timer),&QTimer::timeout,this,&ConnectListModel::m_UpdateModel);
}

QHash<int, QByteArray> ConnectListModel::roleNames() const
{
    QHash<int,QByteArray> buf;
    QMetaEnum role=QMetaEnum::fromType<role_model>();
    for (int i=0,x=0; (x=role.value(i))>=0;i++)
    {
        buf[x]=role.key(i);
    }
    return buf;
}

QVariant ConnectListModel::data(const QModelIndex &index, int role) const
{
    auto buf=cont_Model.at(index.row());
    switch (role) {
    case ip: return buf.value(ip);
    case Alias: return buf.value(Alias);
    case port: return buf.value(port);
    case id: return buf.value(id);
    case rStat: return buf.value(rStat);
    default:
        return QVariant ("Null");
    }
}

//void ConnectListModel::m_roleSetFromEnum()
//{
//    v_rolseEnumStr=QMetaEnum::fromType<my_roles>();
//}

QVariant ConnectListModel::myData(int index, int role)
{
    // return   v_AbstractModelQML[index][role];
}



QList<QString> ConnectListModel::setRoleConnect()
{

}

void ConnectListModel::m_stateChaneged(QString gIP, bool stat)
{
    qDebug () << gIP << "IPchange" << stat << "Status";
    for (auto &i_find: cont_Model)
        if (i_find.value(ip).toString().compare(gIP)==0) {i_find[rStat]=stat; qDebug () <<"newstat"; endResetModel();}
}

void ConnectListModel::m_AppEndRow(QVariant data,QVariant nport)
{
    QHash<int,QVariant> buf;

    buf[ip]=data;
    buf[id]=cont_Model.size();
    buf[Alias]="";
    buf[port]=nport;
    buf[rStat]=0;
    cont_Model.prepend(buf);
    endResetModel();
}
