#ifndef CONNECTLISTMODEL_H
#define CONNECTLISTMODEL_H
#include <../tcpconnectingprograms.h>
#include <QAbstractListModel>
#include <QTimer>
class ConnectListModel : public QAbstractListModel
{
    Q_OBJECT
  //  Q_PROPERTY(QList<int> roleConnect READ roleConnect  NOTIFY roleConnectChanged)
   // Q_PROPERTY(type name READ name WRITE setName NOTIFY nameChanged)WRITE setRoleConnect
public:
    ConnectListModel();
    enum role_model {
        id=Qt::UserRole+1,
        ip,
        port,
        Alias,
        rStat
    };
   Q_ENUM(role_model)
    int rowCount(const QModelIndex &parent) const override {cont_Model.size();}
    QHash <int,QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    Q_INVOKABLE void m_conndect(QString gIP, int gPort) {m_NewCOnnect (gIP,gPort);}
    Q_INVOKABLE void m_StartStopTimer () { if (v_timer.isActive() ) v_timer.stop(); else v_timer.start();}
    Q_INVOKABLE void m_removeRow (int index) {cont_Model.removeAt(index); endResetModel(); }
    Q_INVOKABLE void m_Disconnect (){emit ms_Disconnect();}
signals:
    void ms_Disconnect ();
    void m_NewCOnnect(QString gIP, int gPort) ;
    void roleConnectChanged();
    void ms_UpdateMOdel(int);
private:
    QList<QHash<int,QVariant>> cont_Model;
    QTimer v_timer ;
    int v_CountKey=0;
    // virtual void m_roleSetFromEnum();
    Q_INVOKABLE QVariant myData(int index,int role);
    QList<int> roleConnect () ;
    QList<QString> setRoleConnect();
    Q_INVOKABLE void m_SetTimerInterval (int interval=0) { v_timer.setInterval(interval);}

public slots:
    void m_stateChaneged(QString gIP, bool stat);
    Q_INVOKABLE void m_AppEndRow(QVariant data, QVariant nport);
    void m_UpdateModel(){emit ms_UpdateMOdel((int)TCPQandR::QAllData); }
private slots:

};

#endif // CONNECTLISTMODEL_H
