#ifndef TCPIPSOCKET_H
#define TCPIPSOCKET_H
#include <abstracttcp.h>
#include <QAbstractSocket>
#include <../tcpconnectingprograms.h>

class TcpIpSocket : public AbstractTCP,public GlobalAbstractTCP
{
    Q_OBJECT

public:
    QHash<int ,int> *buf=nullptr;
    TcpIpSocket();
public slots:
    virtual void sl_ConnectTo(QString vHost, int port);
    void m_Query(uint typeQuery) {
        m_SendData(m_TypeToByte(typeQuery));}
    bool m_SendData(QByteArray Byte) {
        if (GlobalAbstractTCP::m_SendData(Byte))
            emit s_EventTCP ("TcpSocket it's closed",Ep::Alarm) ;
#ifdef DebubThis
        emit s_EventTCP ("Data Send",Ep::Event) ;
#endif
        return true;}
    void m_Disconnect(){        v_TcpSocket->disconnectFromHost();
                                disconnect(v_TcpSocket,&QTcpSocket::stateChanged,this,&TcpIpSocket::m_connectStateChange);
                                                        disconnect(v_TcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(sl_ErrorTCP(QAbstractSocket::SocketError)));}
private slots:
    virtual void m_GetData() override {GlobalAbstractTCP::m_GetData();}
    void sl_ErrorTCP(QAbstractSocket::SocketError error);
    void m_QueryGetAll(QHash<QList<int>, QList<QList<int> > > *data);
    void m_connectStateChange (QAbstractSocket::SocketState state)
    {   bool stat=false;
        if (state==QAbstractSocket::ConnectedState) stat=true ;
        emit m_onStateChange(v_TcpSocket->peerAddress().toString(),stat );}
    void ts_GetData();
signals:
    void m_onStateChange(QString sIP, bool State);
    void si_AImodelWrite( QList<QHash<int ,QVariant>> *data);
    void si_AOmodelWrite( QList<QHash<int ,QVariant>> *data);
    void si_DImodelWrite( QList<QHash<int ,QVariant>> *data);
    void si_DOmodelWrite( QList<QHash<int ,QVariant>> *data);
    void si_ALLmodelWrite( QList<QHash<int ,QVariant>> *data);
    void sl_ControllerList( QList<QHash<int ,QVariant>> *data);
private:
    QList<QHash<int ,QVariant>> v_buf;
    virtual void m_ParseGetData(QByteArray data) override;

};

#endif // TCPIPSOCKET_H
