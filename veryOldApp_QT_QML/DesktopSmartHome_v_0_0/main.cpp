#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <event.h>
#include <guiqml.h>
#include <connectlistmodel.h>
#include <alarmmodel.h>
#include <controllerlistmodel.h>
#include <io_model.h>
#include "tcpipsocket.h"
#include <iostream>

//#include <boost/asio.hpp>
//using namespace boost;
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    EventAbstract *o_Event=new Event();
    GuiQml *o_GuiQML=new GuiQml;
    ConnectListModel *Connectlist=new ConnectListModel;
    AlarmAbstract *o_AlarmModel =new AlarmModel;
    ControllerListModel *o_Control=new ControllerListModel;

    IO_model *o_DO_model=new IO_model;
    IO_model *o_AO_model=new IO_model;
    IO_model *o_DI_model=new IO_model;
    IO_model *o_AI_model=new IO_model;
    TcpIpSocket *o_Socket= new TcpIpSocket;
  //  o_Socket->m_sdfsd(&o_AI_model->v_AbstractModelQML);

    o_GuiQML->o_Context->setContextProperty("o_DO_model",o_DO_model);
    o_GuiQML->o_Context->setContextProperty("o_AO_model",o_AO_model);
    o_GuiQML->sl_SetContext("o_DI_model",o_DI_model);
    o_GuiQML->sl_SetContext("o_AI_model",o_AI_model);

    o_GuiQML->sl_SetContext("o_AlarmModel",o_AlarmModel);
    o_GuiQML->o_Context->setContextProperty("ConnectListModel",Connectlist);
    o_GuiQML->sl_SetContext("o_Control",o_Control);

    o_GuiQML->sl_WindowQmlCreate();

    QObject::connect(Connectlist,&ConnectListModel::ms_UpdateMOdel,o_Socket,&TcpIpSocket::m_Query);
   //bool tdd=&ConnectListModel::m_NewCOnnect;
    QObject::connect(o_GuiQML,SIGNAL(si_widthChanged()),o_DO_model,SLOT(sl_dataChanged()));
    QObject::connect(o_Event,SIGNAL(si_ShowEvent(QString,Ep::EventT)),o_GuiQML,SLOT(sl_GetAlarm(QString,Ep::EventT)));
    QObject::connect(Connectlist,&ConnectListModel::m_NewCOnnect,o_Socket,&TcpIpSocket::sl_ConnectTo);
    QObject::connect(o_Socket,SIGNAL(s_EventTCP(QString,Ep::EventT)),o_Event,SLOT(sl_addEvent(QString,Ep::EventT)));
    QObject::connect(o_GuiQML,&GuiQml::si_ItemChange,o_Socket,&TcpIpSocket::m_SendData);
    QObject::connect(o_Event,SIGNAL(si_Alarm(QList<QString>)),o_AlarmModel,SLOT(sl_AddAlarm(QList<QString>)));
    QObject::connect(o_AO_model,&IO_model::m_QueryWrite,o_Socket,&TcpIpSocket::m_SendData);
    QObject::connect(o_DO_model,&IO_model::m_QueryWrite,o_Socket,&TcpIpSocket::m_SendData);
        QObject::connect(o_DO_model,&IO_model::m_Query,o_Socket,&TcpIpSocket::m_Query);

    QObject::connect(o_Socket,SIGNAL(si_AImodelWrite(QList<QHash<int,QVariant> >*)),
                     o_AI_model,SLOT(m_AddIO_ModelData(QList<QHash<int,QVariant> >*)));
    QObject::connect(o_Socket,SIGNAL(si_AOmodelWrite(QList<QHash<int,QVariant> >*)),
                     o_AO_model,SLOT(m_AddIO_ModelData(QList<QHash<int,QVariant> >*)));
    QObject::connect(o_Socket,SIGNAL(si_DImodelWrite(QList<QHash<int,QVariant> >*)),
                     o_DI_model,SLOT(m_AddIO_ModelData(QList<QHash<int,QVariant> >*)));
    QObject::connect(o_Socket,SIGNAL(si_DOmodelWrite(QList<QHash<int,QVariant> >*)),
                     o_DO_model,SLOT(m_AddIO_ModelData(QList<QHash<int,QVariant> >*)));

    QObject::connect(o_Socket,&TcpIpSocket::m_onStateChange,Connectlist,&ConnectListModel::m_stateChaneged);
    QObject::connect(o_GuiQML,SIGNAL(ts_Change()),o_Socket,SLOT(ts_GetData()));
    QObject::connect(Connectlist,&ConnectListModel::ms_Disconnect,o_Socket,&TcpIpSocket::m_Disconnect);
    std::cerr << "dddd";
  //  qDebug () << &TcpIpSocket::m_onStateChange << "MyTest";
    return app.exec();
}
