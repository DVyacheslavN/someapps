#include "event.h"
#include <QDebug>
#include <QDateTime>

Event::Event()
{

}
/*Get the text of the message and the type of output
 *  sent to the GUI class if an emergency message
 *  is sent to the Alarm class*/
void Event::sl_addEvent(QString str, Ep::EventT type)
{
    if (str==nullptr)  {qDebug()<< "Event-> addEvent ->str is null" ; return;}
    if (type==Ep::Alarm)
    {
        QList<QString> buf;
        buf <<str;
        QDateTime data;
        buf << data.currentDateTime().toString("HH:mm -> dd.MM");
        emit si_Alarm(buf);
    }

    emit si_ShowEvent(str ,type);
}
