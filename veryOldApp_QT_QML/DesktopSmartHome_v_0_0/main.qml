import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
ApplicationWindow {

    visible: true
    width:  Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight
    minimumWidth: 600
    title: qsTr("Desktop Control System")

    Item {
        objectName: "root"
        anchors.fill: parent
        Connections{
            target: GUIRelate
            onSi_ShowMessageAlarm:
            {o_MessageAlarm.visible=true
                o_MessageText.text= str
            }
        }
        Connections{
            target:  GUIRelate
            onSi_BottomBarMessage:{
                o_MessageBottomBar.text=str
            }
        }



        Rectangle{
            id: o_MessageAlarm
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: 30
            border.color: "black"
            z: 1
            width: 300
            visible: false
            Text {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                id: o_MessageText
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }
            Button
            {
                anchors.verticalCenter:  parent.verticalCenter
                height: 20
                width: height
                anchors.right: parent.right
                anchors.rightMargin:   parent.height/2 -height/2
                onClicked: parent.visible=false

                icon.name: pressed ? "Error2" : "Error"
                padding: 0
                icon.color:  pressed ? "red" : "transparent"

            }
        }

        MyMenu
        {
            id:o_Menu
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right:  parent.right
            height: 35

        }
        SwipeView {
            id: o_swipeView
            anchors.top: o_Menu.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: o_rect.top
            anchors.topMargin: 20
            currentIndex: 0
            objectName: "oSwipe"

            StartPage {

            }

            ControlPage {

            }
        }
        Rectangle {
            id: o_rect
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 30
            Text{
                id: o_MessageBottomBar
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right:  parent.right
                width: parent.width/5
                text: "AAAA"
            }
        }

        //    footer: TabBar {
        //        id: tabBar
        //        currentIndex: swipeView.currentIndex
        //        TabButton {
        //            text: qsTr("First")
        //        }
        //        TabButton {
        //            text: qsTr("Second")
        //        }
        //    }
    }
}
