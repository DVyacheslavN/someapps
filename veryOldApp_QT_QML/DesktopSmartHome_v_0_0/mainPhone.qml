import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
ApplicationWindow {

    visible: true
    width:  Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight
    minimumWidth: 600
    title: qsTr("Desktop Control System")

    Item {
        objectName: "root"
        anchors.fill: parent
        Connections{
            target: GUIRelate
            onSi_ShowMessageAlarm:
            {o_MessageAlarm.visible=true
                o_MessageText.text= str
            }
        }
        Connections{
            target:  GUIRelate
            onSi_BottomBarMessage:{
                o_MessageBottomBar.text=str
            }
        }



        Rectangle{
            id: o_MessageAlarm
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: 30
            border.color: "black"
            z: 1
            width: 300
            visible: false
            Text {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                id: o_MessageText
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }
            Button
            {
                anchors.verticalCenter:  parent.verticalCenter
                height: 20
                width: height
                anchors.right: parent.right
                anchors.rightMargin:   parent.height/2 -height/2
                onClicked: parent.visible=false

                icon.name: pressed ? "Error2" : "Error"
                padding: 0
                icon.color:  pressed ? "red" : "transparent"

            }
        }

        MyMenu
        {
            id:o_Menu
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right:  parent.right
            height: 35

        }
        SwipeView {
            id: o_swipeView
            anchors.top: o_Menu.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: o_rect.top
            anchors.topMargin: 20
            currentIndex: 0
            objectName: "oSwipe"

            Rectangle{
                Rectangle {
                    id:o_RectTcpIpEdit
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 30
                    // border.color: "black"

                    TextField{
                        id: o_TcpIpEdit
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: parent.width/2
                        //anchors.right: o_butAdd.left
                        // text: "fsdfsd"
                        validator: RegExpValidator {
                            regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
                        }
                    }
                    TextField{
                        id: o_TcpIpNport
                        anchors.left: o_TcpIpEdit.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.right: o_butAdd.left
                    }

                    Button
                    {
                        id: o_butAdd
                        anchors.top: parent.top
                        anchors.right:  parent.right
                        anchors.bottom: parent.bottom
                        width: height
                        //text: "add"
                        onClicked: {
                            //si_Add(o_TcpIpEdit.text)
                            ConnectListModel.m_AppEndRow(o_TcpIpEdit.text,o_TcpIpNport.text)
                            ConnectListModel.m_conndect(o_TcpIpEdit.text,o_TcpIpNport.text)
                        }
                        padding: 0
                        icon.name: o_butAdd.pressed ? "Addp" :  "Addp2"
                        //                background: Image {
                        //                    anchors.fill: parent
                        //                    //name: parent.pressed ? "Add" :  "Add2"
                        //                }

                        icon.color: "transparent"
                    }
                }
                ListView
                {

                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: o_RectTcpIpEdit.bottom
                    anchors.bottom: parent.bottom
                    model: ConnectListModel
                    anchors.margins: 20

                    delegate: Rectangle{
                        width: parent.width
                        height: 35
                        property bool butDo: false

                        TextField {

                            activeFocusOnPress: true
                            anchors.left:  parent.left
                            anchors.right: o_Connect.left
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            id: o_IP_text
                            property var ip_: ip
                            property var my_alias: Alias
                            text: { if (my_alias=="")
                            return ip_
                            else my_alias
                            }
                            property var id_: id
        //                        validator: RegExpValidator {
        //                            regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
        //                        }
                            MouseArea {
                                id: o_MouseIp
                                anchors.fill: parent
                                onDoubleClicked: {
                                    //parent.activeFocusOnPress=true
                                    parent.forceActiveFocus()
                                    enabled=false
                                    o_Connect.visible=false
                                    butDo=true
                                }
                            }
                            onActiveFocusChanged:  {
                                o_MouseIp.enabled=true
                                o_Connect.visible=true
                                //activeFocusOnPress=false
                            }
                            onTextChanged: {
                                var buf=text.text
        //                            if (o_IP_text.text!=ip && )
        //                            {
        //                                o_textBut.text="e"
        //                                butDo=true
        //                            }
        //                            else o_textBut.text= id_
                            }

                        }
                        Button
                        {

                            id:o_textBut
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            width: parent.height
                            //text: "d"
                            onClicked: {
                                o_Connect.visible=true
                                if (butDo) { ConnectListModel.setData(ConnectListModel.index(index,0),o_IP_text.text,ConnectListModel.roleConnect[2]) }
                                else {ConnectListModel.m_removeRow(index)}

                               // o_textBut.text="de"
                                butDo=false
                                forceActiveFocus()
                                //o_IP_text.undo()
                                o_MouseIp.enabled=true
                            }

        //                    background:  Image {
        //                        id: image1
        //                        anchors.fill: parent
        //                        source: parent.pressed ? "iconDeleteBlack" : "iconDeleteWhite"

        //                    }

                            icon.name: pressed ? "Delete" : "Delete2"
                            icon.color: "transparent"
                            padding: 0
        //                    background: Rectangle
        //                    {
        //                        color: "white"
        //                    }

                        }
                        Button{

                            id: o_Connect
                            anchors.right: o_textBut.left
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            width: height


                           // text: "C"
                            onClicked: {
                                ConnectListModel.m_conndect(ip,port);
                                //si_ConnnectTO(o_IP_text.text)
                            }
        //                    background:
        //                        Image {
        //                        anchors.fill: parent
        //                        //name: rStat || parent.pressed ? "Connect" : "Disconnect"

        //                    }
                            icon.name : rStat || pressed ? "Connect" : "Disconnect"
                            padding: 0
                            icon.color: "transparent"

                        }
                    }
                }
            }

            Rectangle {
                Flickable {
                    clip: true
                    id: o_RectCentrStatusChanal
                    // border.color: "red"
                    anchors.fill: parent

                    //contentWidth: parent.width * 2
                    contentHeight: o_redffds.height

                    //ScrollBar.horizontal: ScrollBar { id: hbar; active: vbar.active }
                    ScrollBar.vertical: ScrollBar { id: vbar; //active: hbar.active
                    }
                    //anchors.margins: 20
            //        ScrollBar {
            //            id: vbar
            //            hoverEnabled: true
            //            active: hovered || pressed
            //            orientation: Qt.Vertical
            //            size: o_RectCentrStatusChanal.height / o_redffds.height
            //            anchors.top: parent.top
            //            anchors.right: parent.right
            //            anchors.bottom: parent.bottom
            //        }
                    Rectangle{
                        id: o_redffds
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.right: parent.right

                        height: o_RectAO.height+o_RectDO.height+o_RectDI.height+o_RectAI.height

                    Rectangle {

                        id: o_RectDO
                        property var l_cellWidth: 50
                        property var l_cellHeight: l_cellWidth / (96/48)
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        property int v_itemToRow: { ((o_RectCentrStatusChanal.width-(o_RectCentrStatusChanal.width
                                                                                     %l_cellWidth))/l_cellWidth)}
                        property int v_rowCount: {
                            var x=0
                            var m=0;
                            if ((m=(o_DO_model.v_siseRow%v_itemToRow))>0) x=1
                            return (o_DO_model.v_siseRow-m)/v_itemToRow+x
                        }
                        width: v_itemToRow*l_cellWidth
                        height: v_rowCount*l_cellHeight
                        //border.color: "black"
            //            Text {
            //                id: o_NameDO
            //                anchors.top: parent.top
            //                height: 20
            //                anchors.horizontalCenter:  parent.horizontalCenter

            //                text: qsTr("DO")
            //            }
                        GridView
                        {
                            interactive: false

                            id: o_ViewDoModel
                            //anchors.topMargin: l_cellHeight/2
                            anchors.fill: parent

                            height: parent.height
                            width: parent.width
                            model: o_DO_model
                            cellWidth: o_RectDO.l_cellWidth
                            cellHeight: o_RectDO.l_cellHeight
                            delegate: Rectangle {
                                id: o_Deleg
                                height: o_RectDO.l_cellHeight
                                width: o_RectDO.l_cellWidth
                                Button{
                                  //  padding: 0
                                    leftPadding: 0
                                    rightPadding: 0
                                    topPadding: 0
                                    bottomPadding: 0
                                    //transform:  Scale { origin.x: 0; origin.y: 20; xScale: 0.7; yScale: 0.7; }
                                    anchors.fill: parent

                                   // anchors.left: parent.left
                                    //anchors.verticalCenter: parent.verticalCenter


                                    onPressAndHold: {
                                        console.log("fix")

                                    }
                                    //checked: {status}
                                    onClicked:  { //if (o_DO_model.status!=1)
                                        //si_newStatus(id,checked ? 1 : 0)

                                        o_DO_model.ms_DataChange(index, rStatus ? 0 : 1);
                                    }
                                    background: Image {
                                            anchors.verticalCenter: parent.verticalCenter
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            width: o_RectDO.l_cellWidth-5
                                            height: o_RectDO.l_cellHeight-2.5
                                        source: rStatus ? "icons/gallery/10x10/On.png" : "icons/gallery/10x10/Off.png"
                                    }
            //                        icon{
            //                            source: "iconToggleOn"

            //                        }
                                   // icon.name: rStatus ? "On" : "Off"

                                  //  icon.color: "transparent"
                                }
                            }
                        }
                    }
                    Rectangle {
                        id: o_RectAO
                        property int l_cellWidth: 100
                        property int l_cellHeight: 28
                        anchors.top: o_RectDO.bottom
                            //            anchors.left: parent.left
                        anchors.horizontalCenter: parent.horizontalCenter

                        //anchors.right: parent.right
                        property int v_itemToRow: { ((o_RectCentrStatusChanal.width-(o_RectCentrStatusChanal.width
                                                                                     %o_ViewAOModel.cellWidth))/o_ViewAOModel.cellWidth)}
                        property int v_rowCount: {
                            var x=0
                            var m=0;
                            if ((m=(o_AO_model.v_siseRow%v_itemToRow))>0) x=1
                            return (o_AO_model.v_siseRow-m)/v_itemToRow+x
                        }
                        width: v_itemToRow*o_ViewAOModel.cellWidth
                        height: v_rowCount*o_ViewAOModel.cellHeight
                       // border.color: "black"
                        GridView
                        {
                             interactive: false
                            id: o_ViewAOModel
                            //anchors.topMargin: cellHeight/2
                            anchors.fill: parent
            //                height: parent.height
            //                width: parent.width
                            cellHeight: o_RectAO.l_cellHeight
                            cellWidth: o_RectAO.l_cellWidth
                            model: o_AO_model
                            delegate: Rectangle {
                                id: o_DelegAO
                                height: o_RectAO.l_cellHeight
                                width: o_RectAO.l_cellWidth
                                //anchors.left: parent.left

                                TextField
                                {
                                    id: l_writeText
                                    anchors.left: parent.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom


                                  // height: o_ViewAOModel.cellHeight
                                    width:  o_ViewAOModel.cellWidth-o_ViewAOModel.cellHeight
                                    text: rStatus
                                }
                                Button {

                                    anchors{
                                        right: parent.right
                                        top: parent.top
                                        bottom: parent.bottom
                                    }
                                    width: height

                                    background: Rectangle
                                    {
                                        width: parent.width
                                        height: parent.height
                                        radius: parent.width/2
                                        color: parent.pressed ? "#cacaca" :"#9c9c9c"
                                    }
                                    onClicked:
                                    {
                                         o_AO_model.ms_DataChange(index, l_writeText.text);
                                    }
                                }
                            }
                        }

                    }
                    Rectangle {
                        id: o_RectDI
                        anchors.top: o_RectAO.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        property int v_itemToRow: { ((o_RectCentrStatusChanal.width-(o_RectCentrStatusChanal.width
                                                                                     %o_ViewDIModel.cellWidth))/o_ViewDIModel.cellWidth)}
                        property int v_rowCount: {
                            var x=0
                            var m=0;
                            if ((m=(o_DI_model.v_siseRow%v_itemToRow))>0) x=1
                            return (o_DI_model.v_siseRow-m)/v_itemToRow+x
                        }
                        width: v_itemToRow*o_ViewDIModel.cellWidth
                        height: v_rowCount*o_ViewDIModel.cellHeight
                       // border.color: "black"
                        Text {
                            id: o_NameDI
                            anchors.top: parent.top
                            height: 20
                            anchors.horizontalCenter:  parent.horizontalCenter

                            text: qsTr("DO")
                        }
                        GridView
                        {
                             interactive: false
                            id: o_ViewDIModel
                            anchors.topMargin: cellHeight/2
                            anchors.fill: parent

                            height: parent.height
                            width: parent.width
                            model: o_DI_model
                            cellWidth: 100
                            cellHeight: 25
                            delegate: Rectangle {
                                id: o_DelegDI
                                height: parent.cellHeight
                                width: parent.cellWidth
                                border.color: "black"
                                Switch{
                                    transform:  Scale { origin.x: 0; origin.y: 20; xScale: 0.7; yScale: 0.7; }

                                    anchors.left: parent.left
                                    anchors.verticalCenter: parent.verticalCenter

                                    onPressAndHold: {
                                        console.log("fix")

                                    }
                                    checked: {rStatus}
                                    onClicked:  { //if (o_DO_model.status!=1)
                                        si_newStatus(id,checked ? 1 : 0)
                                    }
                                }
                            }
                        }
                    }
                    Rectangle {
                        id: o_RectAI
                        anchors.top: o_RectDI.bottom
                            //            anchors.left: parent.left
                        anchors.horizontalCenter: parent.horizontalCenter

                        //anchors.right: parent.right
                        property int v_itemToRow: { ((o_RectCentrStatusChanal.width-(o_RectCentrStatusChanal.width
                                                                                     %o_ViewAIModel.cellWidth))/o_ViewAIModel.cellWidth)}
                        property int v_rowCount: {
                            var x=0
                            var m=0;
                            if ((m=(o_AI_model.v_siseRow%v_itemToRow))>0) x=1
                            return (o_AI_model.v_siseRow-m)/v_itemToRow+x
                        }
                        width: v_itemToRow*o_ViewAIModel.cellWidth
                        height: v_rowCount*o_ViewAIModel.cellHeight
                       // border.color: "black"
                        GridView
                        {
                             interactive: false
                            id: o_ViewAIModel
                            anchors.topMargin: cellHeight/2
                            anchors.fill: parent

                            height: parent.height
                            width: parent.width
                            model: o_AI_model
                            cellWidth: 100
                            cellHeight: 30
                            delegate: Rectangle {
                                id: o_DelegAI
                                height: parent.cellHeight
                                width: parent.cellWidth
                                border.color: "black"

                                TextField
                                {
                                  //  anchors.fill: parent
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.left: parent.left
                                    height: o_ViewAIModel.cellHeight
                                    width:  o_ViewAIModel.cellWidth
                                    text: rStatus
                                }
                            }
                        }

                    }
                }
            }
        }
        }
        Rectangle {
            id: o_rect
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 30
            Text{
                id: o_MessageBottomBar
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right:  parent.right
                width: parent.width/5
                text: "AAAA"
            }
        }

        //    footer: TabBar {
        //        id: tabBar
        //        currentIndex: swipeView.currentIndex
        //        TabButton {
        //            text: qsTr("First")
        //        }
        //        TabButton {
        //            text: qsTr("Second")
        //        }
        //    }
    }
}
