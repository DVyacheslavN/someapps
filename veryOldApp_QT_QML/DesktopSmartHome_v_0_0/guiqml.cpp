#include "guiqml.h"
#include <QDebug>
#include <io_model.h>
#include <QHash>
#include <QQuickStyle>
#include <QIcon>
#include <QSettings>
//#include
GuiQml::GuiQml()
{


      QIcon::setThemeName("gallery");
    o_Engine= new QQmlApplicationEngine;
    o_Context= o_Engine->rootContext();
   // QIcon::setThemeSearchPaths();

     // QIcon::setThemeName("mytheme");

    o_Context->setContextProperty("GUIRelate",this);
    // qDebug () << o_Engine->rootObjects();
    //QObject*obj= .at(0);
    //
}

void GuiQml::sl_WindowQmlCreate()
{

#ifdef DebubThis//ANDROID
    o_Engine->load(QUrl(QLatin1String("qrc:/mainPhone.qml")));
#else
    o_Engine->load(QUrl(QLatin1String("qrc:/main.qml")));
    QList<QObject*> root=o_Engine->rootObjects();
    // v_RootObject=root[0];
    v_RootObject= sl_ObjecQmlRequested("root/oSwipe/oStart");
    qDebug () << v_RootObject << "RootObj";
    // qDebug () << o_Engine->rootObjects().at(0)->findChild<QObject*>("o_swipeView");
    //     v_RootObject = o_Engine->rootObjects().at(0);
    connect(v_RootObject,SIGNAL(si_ConnnectTO(QString, int )),this,SIGNAL(si_ConnectTo(QString,int)));
    connect(v_RootObject,SIGNAL(ts_Change()),this,SIGNAL(ts_Change()));

    v_RootObject= sl_ObjecQmlRequested("oSwipe/oControl");
    connect(v_RootObject,SIGNAL(si_newStatus(int,int)),this,SLOT(sl_ItemChange(int,int)));
#endif

}

void GuiQml::sl_SetContext(QString nameContext, QObject *objectContext)
{
    o_Context->setContextProperty(nameContext,objectContext);

}

QObject *GuiQml::sl_ObjecQmlRequested(QString Path)
{
    QObject * bufReturn=nullptr;
    if (1) {
        QList<QObject*> root=o_Engine->rootObjects();

        QRegExp reg ("[a-zA-Z0-9]{1,9}");
        int iterator=0;
        bufReturn=root[0];
        while (((iterator=reg.indexIn(Path,iterator))!=-1) && bufReturn!=nullptr) {
            iterator+=reg.matchedLength();
            bufReturn=bufReturn->findChild<QObject*>(reg.cap(0));
        }
        // emit si_ObjecQmlSending(bufReturn);
    }
    else qDebug () << "Isn't created EngineObject";
    return bufReturn;
}

void GuiQml::sl_AddItemConnectList(QString str)
{
    qDebug () << "SlotItemConnectList";
    //emit si_AddItemConnectList(str);
}

void GuiQml::sl_ConnectIndex(QString IP, int vIndex)
{
    qDebug () << IP << vIndex;
}

void GuiQml::sl_GetAlarm(QString str, Ep::EventT type)
{
    if (type== Ep::Alarm) emit si_ShowMessageAlarm(str);
    emit si_BottomBarMessage(str);
}

void GuiQml::ts_text(QString data, int x)
{
    emit si_ShowMessageAlarm(data + QString::number(x,10));
}

void GuiQml::sl_ItemChange(int id,int stat)
{

    qDebug () << id << "id " << stat << "Stat";
    QList <int> buf;
    buf << id << stat << 0;
    QVariant var;
    var.setValue<QList<int>>(buf);
    qDebug () << var << "ListToVariant";
    emit si_ItemChange(Ep::m_TforByte(Ep::intem_Change,buf));
}
