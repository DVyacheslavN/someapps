#ifndef GUIQML_H
#define GUIQML_H
#include <guimyabstract.h>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <desctopprojenums.h>
class GuiQml : public GuiQMLAbstract
{
    Q_OBJECT
public:
    explicit GuiQml();
    virtual void sl_WindowQmlCreate();
    virtual void sl_SetContext(QString nameContext, QObject *objectContext);
    QQmlContext *o_Context;
private:

    QQmlApplicationEngine *o_Engine;
    QObject *v_RootObject;

    QObject * sl_ObjecQmlRequested(QString Path); /* return the obj2 from signal*/
signals:
    // void  si_ShowMessage(QString str);
    void si_widthChanged();
    void si_ItemChange(QByteArray data);
    void ts_Change();
private slots:
    void sl_AddItemConnectList(QString str);

    void sl_ItemChange(int id, int stat);
public slots:
    void sl_ConnectIndex(QString IP,int vIndex);
    void sl_GetAlarm(QString str, Ep::EventT type);
    void ts_text(QString data,int x);

};

#endif // GUIQML_H
