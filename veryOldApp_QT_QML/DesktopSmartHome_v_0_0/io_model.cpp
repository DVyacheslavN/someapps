#include "io_model.h"
#include <QDebug>

IO_model::IO_model()
{
    //    m_roleSetFromEnum();
    //    int col=1;
//    int row=5;
//    for (int i=0;i<row;i++)
//    {
//        QHash<int,QVariant> buf;
//        buf[G_IO_MODEL::id] = i;
//        // buf.replace(m_Alias,"fsdfs";
//        buf[G_IO_MODEL::rStatus]= (i%2==0) ? 1 :0 ;

//        {
//            v_IO_modelData.append(buf);
//        }
//    }
}

QHash<int, QByteArray> IO_model::roleNames() const
{
    QHash<int, QByteArray> role;
    QMetaEnum buf =QMetaEnum::fromType<G_IO_MODEL::m_IO_Role>();
    for (int i=0,m=0; (m=buf.value(i))>=0;i++)
    {
        role[m]=buf.key(i);
    }

    return role;
}

QVariant IO_model::data(const QModelIndex &index, int role) const
{

    auto l_buf= v_IO_modelData.at(index.row());
    if (role==G_IO_MODEL::rStatus) return QVariant(l_buf.value(G_IO_MODEL::rStatus));
    if (role==G_IO_MODEL::id) return QVariant( l_buf.value(G_IO_MODEL::id));
    if (role==G_IO_MODEL::adrContr) return QVariant(l_buf.value(G_IO_MODEL::adrContr));
    if (role==G_IO_MODEL::TypeReg) return QVariant( l_buf.value(G_IO_MODEL::TypeReg));
    return QVariant ("NUll");
}


void IO_model::sl_dataChanged()
{

    emit dataChanged(QModelIndex(),QModelIndex());
    emit endResetModel();
    qDebug () << "dataChanged";
}

void IO_model::m_AddRow(QList<int> gData)
{

}

void IO_model::m_AddIO_ModelData(QList<QHash<int, QVariant> > *gModelData)
{
    //v_IO_modelData.clear();

    if (v_IO_modelData.size() != gModelData->size())
    {
        v_IO_modelData=gModelData->toVector().toStdVector();
        endResetModel();
        emit v_siseRowChanged();
        return;
    }
    for (int i=0; i<gModelData->size(); i++)
    {
        if (v_IO_modelData.at(i)[G_IO_MODEL::rStatus].toInt() !=
                gModelData->at(i)[G_IO_MODEL::rStatus].toInt() )
        {

            qDebug () << "update " << i;
            v_IO_modelData[i][G_IO_MODEL::rStatus]=gModelData->at(i)[G_IO_MODEL::rStatus];
            dataChanged(index(i),index(i));

        }
    }

   // endResetModel();

}

int IO_model::v_siseRow()
{
    return v_IO_modelData.size();
}

void IO_model::ms_DataChange(int index, int l_newValue)
{
    auto buf =v_IO_modelData.at(index);
    buf[G_IO_MODEL::rStatus]=l_newValue;

    emit m_QueryWrite(  GlobalAbstractTCP::m_TypeToByte((int)TCPQandR::QWriteReg,buf));
    //        // emit m_Query(TCPQandR::QWriteReg);
    //    }
}
