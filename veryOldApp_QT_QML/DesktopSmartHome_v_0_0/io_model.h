#ifndef IO_MODEL_H
#define IO_MODEL_H
#include <desctopprojenums.h>
#include <QAbstractListModel>
#include <../tcpconnectingprograms.h>
#include <../globalmodel.h>

class IO_model : public QAbstractListModel
{

    Q_OBJECT
    Q_PROPERTY(int  v_siseRow READ v_siseRow  NOTIFY v_siseRowChanged)
public:
    IO_model();
    virtual int rowCount(const QModelIndex &parent) const override {v_IO_modelData.size();}
    virtual QHash<int ,QByteArray> roleNames() const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
private:
    std::vector<IO_Model_WriteTYpe> v_IO_modelData;
public slots:
    void ms_DataChange(int index , int l_newValue);
    void sl_dataChanged();
    void m_AddRow (QList<int> gData);
    void m_AddIO_ModelData (QList<QHash<int, QVariant> > *gModelData) ;
    virtual int v_siseRow();
signals:
    void v_siseRowChanged();

    void m_QueryWrite(QByteArray byte);
    void m_Query(int);
};


#endif // DO_MODEL_H
