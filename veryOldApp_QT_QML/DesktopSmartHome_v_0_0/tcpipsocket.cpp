#include "tcpipsocket.h"
#include <QDataStream>
#include <QList>
#include <QHash>
#include <../globalmodel.h>
TcpIpSocket::TcpIpSocket()
{
    v_TcpSocket=new QTcpSocket;
    tcp.setDevice(v_TcpSocket);
    connect(this,SIGNAL(si_ConectRedy()),this,SLOT(pr_connect()));
    connect(v_TcpSocket,SIGNAL(readyRead()),this,SLOT(m_GetData()));
    connect(v_TcpSocket,SIGNAL(connected()),this,SLOT(sl_Connected()));
    if (!v_TcpSocket->isValid()) { emit s_EventTCP("AlarmSocket", Ep::Alarm); return;}
    v_buf.clear();
}

void TcpIpSocket::sl_ConnectTo(QString vHost, int port)
{
    qDebug () << "Conndect start";
    v_TcpSocket->disconnectFromHost();
    v_TcpSocket->close();
    if (v_TcpSocket->state()==QAbstractSocket::ConnectingState)
    { }
    qDebug () << v_TcpSocket->state()<< "conState";
    //v_TcpSocket->connectToHost(vHost,port);
    v_TcpSocket->connectToHost(vHost,port);
    connect(v_TcpSocket,&QTcpSocket::stateChanged,this,&TcpIpSocket::m_connectStateChange);
    connect(v_TcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(sl_ErrorTCP(QAbstractSocket::SocketError)));
    //   pr_connect();
    emit si_ConectRedy();
}


void TcpIpSocket::sl_ErrorTCP(QAbstractSocket::SocketError error)
{
    switch (error) {
    case QAbstractSocket::ConnectionRefusedError:
        emit s_EventTCP("ErrorConnect <",Ep::Alarm);
        m_Disconnect();
        break;
    default:
        break;
    }
}


void TcpIpSocket::ts_GetData()
{
    qDebug () << "Change TcpGET";
    QList<QHash<int ,QVariant>> buf;
    QHash<int ,QVariant> buf2;
    for (int i=0;i<5;i++)
    {
        buf2[257]=1;
        buf2[258]=1;
        buf2[259]=i;
        buf.append(buf2);
    }
    QVariant var;
    var.setValue <QList<QHash<int, QVariant>>> (buf);
    //sl_ExtractType(Ep::AImodel,var);
}

void TcpIpSocket::m_ParseGetData(QByteArray data)
{
    QDataStream l_Stream(data);
    quint16 type;
    l_Stream >> type;
    switch (type) {
    case (int)TCPQandR::AAllData:
    {
        qDebug() << "get Ansver All";
        QHash<QList<int>, QList<QList<int>>> buf;
        l_Stream >> buf;
        m_QueryGetAll(&buf);
    }
        // m_SendData(m_TforByte(TCPQandR::AAllData,m_GetAllBack()));
        break;
    default:
        qDebug() << "Query";
        break;
    }
}

void TcpIpSocket::m_QueryGetAll(QHash<QList<int>, QList<QList<int>>> *data)
{

    //    buf[21]="mVal 21";
    //    buf[id]=2;
    //    buf[Alias]="motor";
    //    buf[status]= 0;
    //    buf[Type]= Et::A_DO;

    //    adrCell,status,Alias,Type
    /*type register  */
    QHash<int,QVariant> buf;
    QList <QHash<int,QVariant>> bufDO;
    QList <QHash<int,QVariant>> bufAO;
    /*format sender  key (idAdressCOntroller, PortID) , Value (AdressCel,Status,Alisas,Type)   */
    int i=0;
    for (auto l_find: data->keys())
    {
        for (auto j_find : data->value(l_find))
        {
            buf.clear();
            buf[G_IO_MODEL::id]=i;
            buf[G_IO_MODEL::adrContr]=l_find.at(0);
            buf[G_IO_MODEL::m_Alias]="motor";
            buf[G_IO_MODEL::rStatus]=j_find.at(1);
            buf[G_IO_MODEL::port]=l_find.at(1);
            buf[G_IO_MODEL::TypeReg]=j_find.at(3);
            buf[G_IO_MODEL::adrCell]=j_find.at(0);
            if (j_find.at(3)>2)
                bufAO.append(buf);
            else bufDO.append(buf);
            i++;
        }
    }



    // QList<QHash<int ,QVariant>>
    //    QList <QHash<int,QVariant>> bufAI=(data->value(Ep::A_AI));
    //    QList <QHash<int,QVariant>> bufAO=(data->value(Ep::A_AO));
    //    QList <QHash<int,QVariant>> bufDI=(data->value(Ep::A_DI));
    //    QList <QHash<int,QVariant>> bufDO=(data->value(Ep::A_DO));
    //    emit si_AImodelWrite(&bufAI);
    emit si_AOmodelWrite(&bufAO);
    //    emit si_DImodelWrite(&bufDI);
    emit si_DOmodelWrite(&bufDO);
}




//template<typename T>
//void TcpIpSocket::m_send(Ep::TcpT type, T data)
//{
//    if (v_TcpSocket==NULL || !v_TcpSocket->isValid())
//    {
//        emit si_EventTCP("TcpSocket it's closed", Ep::Alarm);
//        qDebug () << data << "Tcp data Send";
//        return ;
//    }
//    // созданим массв куда будем загонять данные

// //   v_TcpSocket->write(byte);
//  //  data.clear();
//    emit si_EventTCP("DataSend",Ep::Event);
//}


