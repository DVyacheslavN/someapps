#ifndef ALARMABSTRACT_H
#define ALARMABSTRACT_H
#include <QAbstractTableModel>
#include <abstractmodelqml.h>
class AlarmAbstract  : public AbstractModelQML
{
    Q_OBJECT
public:
    AlarmAbstract() {}

    virtual void sl_DeleteItem (int position)=0;
    virtual void sl_Confirm (int position)=0;
public slots:
    virtual void sl_AddAlarm (QList<QString>str) =0;

};
#endif // ALARMABSTRACT_H
