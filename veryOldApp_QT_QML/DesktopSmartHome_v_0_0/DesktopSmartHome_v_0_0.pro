QT += quick network quickcontrols2
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    event.cpp \
    guiqml.cpp \
    abstractmodelqml.cpp \
    connectlistmodel.cpp \
    alarmmodel.cpp \
    controllerlistmodel.cpp \
    io_model.cpp \
    abstracttcp.cpp \
    tcpipsocket.cpp

RESOURCES += qml.qrc \
    icons/gallery/index.theme \
    $$files(icons/*.png,true)
   # $$files(images/*.png)

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
INCLUDEPATH +=/home/slava/QtProj/boost_1_66_0/

HEADERS += \
    eventabstract.h \
    event.h \
    guiqml.h \
    guimyabstract.h \
    alarmabstract.h \
    abstractmodelqml.h \
    connectlistmodel.h \
    alarmmodel.h \
    controllerlistmodel.h \
    io_model.h \
    abstracttcp.h \
    tcpipsocket.h \
    desctopprojenums.h \
    ../globalmodel.h

DISTFILES +=
