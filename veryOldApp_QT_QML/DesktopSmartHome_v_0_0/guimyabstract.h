#ifndef GUIMYABSTRACT_H
#define GUIMYABSTRACT_H
#include <QObject>
#include <QString>
#include <desctopprojenums.h>


class GuiQMLAbstract : public QObject
{
    Q_OBJECT
public:
    GuiQMLAbstract() {}
    Q_INVOKABLE virtual void sl_SetContext(QString nameContext, QObject *objectContext)=0;
    Q_INVOKABLE virtual void sl_WindowQmlCreate()=0;
    Q_SLOT virtual void sl_GetAlarm(QString ,Ep::EventT)=0;
Q_SIGNALS:
      void si_DeleteItemConnectList(int position);
      void si_ConnectTo(QString IP,int nPort);
      void si_SetAlias(QString *alias);
      void si_SetNewStatusItem(int newStatus);
      void si_ShowMessageAlarm(QString str);
      void si_BottomBarMessage(QString str);
      void si_AddItemConnectList(QString str);
};
#endif // GUIQML_H

