#include "abstractmodelqml.h"
#include <QDebug>
AbstractModelQML::AbstractModelQML(QObject *parent)
{
    m_roleSetFromEnum();
    //    v_rolseEnumStr=QMetaEnum::fromType<my_roles>();
    //    for (int i=0;i<row;i++)
    //    {
    //        for (int j=0;j<col;j++)
    //        {
    //            v_AbstractModelQML[i][Qt::UserRole+1+j]=i+j;
    //        }
    //    }

}

int AbstractModelQML::rowCount(const QModelIndex &parent) const
{
    return v_AbstractModelQML.size();
}

int AbstractModelQML::columnCount(const QModelIndex &parent) const
{
    return v_AbstractModelQML.at(0).size();
}

QVariant AbstractModelQML::data(const QModelIndex &index, int role) const
{

    return  v_AbstractModelQML.at(index.row()).value(role);
}

bool AbstractModelQML::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid()
            && value.isValid() )
    {
        v_AbstractModelQML[index.row()][status]=value;
        dataChanged(index,index);
         endResetModel();
        return true;
    }
    return false;
}

Qt::ItemFlags AbstractModelQML::flags(const QModelIndex &index) const
{

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

QHash<int, QByteArray> AbstractModelQML::roleNames() const
{
    QHash <int, QByteArray> roles=QAbstractTableModel::roleNames();
    int x=0;
    for (int i=0 ; (x=v_rolseEnumStr.value(i))>0 ;i++) {
        roles[x]=v_rolseEnumStr.key(i);
    }
    return roles;
}

QModelIndex AbstractModelQML::index(int row, int column, const QModelIndex &parent) const
{
    return QAbstractTableModel::createIndex(row,column);
}

void AbstractModelQML::m_roleSetFromEnum()
{
    v_rolseEnumStr=QMetaEnum::fromType<ALLrole>();
}

void AbstractModelQML::removeRow(int row, const QModelIndex &parent)
{

    v_AbstractModelQML.removeAt(row);
//    QHash<int, QHash<int, QVariant>>::iterator it= v_AbstractModelQML.begin();
//    for (int i=0;i<row && it!=v_AbstractModelQML.end();i++) it++;
//   qDebug () <<  it.key();
//   // v_AbstractModelQML.erase(it);
//    v_AbstractModelQML.remove(it.key());

//    qDebug () << row << "delete row";
//     qDebug () << v_AbstractModelQML.keys() << "to keys";
//      qDebug () << v_AbstractModelQML[row][258];
//   v_AbstractModelQML.remove(row);
//   qDebug ()<< v_AbstractModelQML.keys()<< "do keys";
//   qDebug () << v_AbstractModelQML.value(row);

    endResetModel();
}

QVariant AbstractModelQML::m_data(int row,int role)
{
//    QHash<int, QHash<int, QVariant>>::iterator it= v_AbstractModelQML.begin();
//    for (int i=0;i<row && it!=v_AbstractModelQML.end();i++) it++;
    //return it.value().value(role);
}

int AbstractModelQML::v_siseRow()
{
    return v_AbstractModelQML.size();
}

void AbstractModelQML::sl_WriteAll(QList<QHash<int, QVariant> > * data)
{
    qDebug() << v_AbstractModelQML.size() << "SIZE_MOD";
    v_AbstractModelQML.clear();
    v_AbstractModelQML << (*data);
    endResetModel();
    qDebug() << "write model";
    qDebug() << v_AbstractModelQML.size();
    emit v_siseRowChanged();
}
