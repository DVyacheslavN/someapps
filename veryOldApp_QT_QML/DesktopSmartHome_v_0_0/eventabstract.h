#ifndef EVENTABSTRACT_H
#define EVENTABSTRACT_H
//#include <QMetaObject>
#include <QString>
#include <QMetaObject>
#include <QObject>
#include <desctopprojenums.h>
class EventAbstract : public QObject
{
    Q_OBJECT
public:

    explicit EventAbstract() {}
public slots:
     virtual void sl_addEvent (QString str,Ep::EventT type)=0;
signals:
     void si_Alarm (QList<QString> str);
     void si_ShowEvent (QString str, Ep::EventT);
};



#endif // EVENTABSTRACT_H
