import QtQuick 2.0
import QtQuick.Controls 2.0
//import QtQuick.Controls 1.4
Item {
    objectName: "oStart"
    signal  si_ConnnectTO(string str, int nport)
    signal ts_Change()
    Rectangle{
        anchors.fill: parent
        // visible: !o_Message.visible

        Rectangle{
            anchors.leftMargin: 30
            anchors.rightMargin: 20
            id: o_RectConnectList
            // border.color: "black"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width:  200
            ListView
            {
                anchors.fill: parent
                model: ConnectListModel
                interactive: false
                delegate: Rectangle{
                    width: parent.width
                    height: 25
                    property bool butDo: false

                    TextField {
                        activeFocusOnPress: true
                        anchors.left:  parent.left
                        anchors.right: o_Connect.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        id: o_IP_text
                        property var ip_: ip
                        property var my_alias: Alias
                        text: { if (my_alias=="")
                                return ip_
                            else my_alias
                        }
                        property var id_: id
                        //                        validator: RegExpValidator {
                        //                            regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
                        //                        }
                        MouseArea {
                            id: o_MouseIp
                            anchors.fill: parent
                            onDoubleClicked: {
                                //parent.activeFocusOnPress=true
                                parent.forceActiveFocus()
                                enabled=false
                                o_Connect.visible=false
                                butDo=true
                            }
                        }
                        onActiveFocusChanged:  {
                            o_MouseIp.enabled=true
                            o_Connect.visible=true
                            //activeFocusOnPress=false
                        }
                        onTextChanged: {

                            var buf=text.text
                            //                            if (o_IP_text.text!=ip && )
                            //                            {
                            //                                o_textBut.text="e"
                            //                                butDo=true
                            //                            }
                            //                            else o_textBut.text= id_
                        }

                    }
                    Button
                    {
                        id:o_textBut
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: parent.height
                        // text: "d"
                        onClicked: {
                            o_Connect.visible=true
                            if (butDo) { ConnectListModel.setData(ConnectListModel.index(index,0),o_IP_text.text,Alias) }
                            else {ConnectListModel.m_removeRow(index)}

                            // o_textBut.text="de"
                            butDo=false
                            forceActiveFocus()
                            //o_IP_text.undo()
                            o_MouseIp.enabled=true

                        }

                        //                        background:  Image {
                        //                            id: image1
                        //                            anchors.fill: parent
                        //                            source: parent.pressed ? "icon/DeleteBlack" : "icon/DeleteWhite"

                        //                        }
                        padding: 0
                        icon.name: pressed ? "Delete" : "Delete2"
                        icon.color: "transparent"
                    }
                    Button{
                        id: o_Connect
                        anchors.right: o_textBut.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: height
                        property var stat: rStat
                        //text: "C"
                        onClicked: {
                            if (rStat) ConnectListModel.m_Disconnect()
                                else
                            ConnectListModel.m_conndect(ip,port)
                            //si_ConnnectTO(ip,port)
                        }
                        //                        background:
                        //                            Image {
                        //                            anchors.fill: parent
                        //                            name: rStat || parent.pressed ? "Connect" : "Disconnect"

                        //                        }
                        icon{
                            name: (rStat || o_Connect.pressed) ? "Connect" : "Disconnect"
                        }
                        icon.color: "transparent"
                        padding:0

                    }
                }
            }
        }
        Rectangle {
            id:o_RectTcpIpEdit
            anchors.leftMargin: 20
            anchors.top: parent.top
            anchors.left: o_RectConnectList.right
            height: 30
            // border.color: "black"
            width: 400
            TextField{
                id: o_TcpIpEdit
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width/2
                //anchors.right: o_butAdd.left
                // text: "fsdfsd"
                validator: RegExpValidator {
                    regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
                }
            }
            TextField{
                id: o_TcpIpNport
                anchors.left: o_TcpIpEdit.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: o_butAdd.left
            }

            Button
            {
                id: o_butAdd
                anchors.top: parent.top
                anchors.right:  parent.right
                anchors.bottom: parent.bottom
                width: height
                //text: "add"
                onClicked: {
                    //si_Add(o_TcpIpEdit.text)
                    ConnectListModel.m_AppEndRow(o_TcpIpEdit.text,o_TcpIpNport.text)
                    ConnectListModel.m_conndect(o_TcpIpEdit.text,o_TcpIpNport.text)
                }
                padding: 0
                icon.name: o_butAdd.pressed ? "Addp" :  "Addp2"
                //                background: Image {
                //                    anchors.fill: parent
                //                    //name: parent.pressed ? "Add" :  "Add2"
                //                }

                icon.color: "transparent"
            }
        }
        Rectangle
        {
            anchors.leftMargin: 20
            anchors.topMargin: 20
            id:o_RectControllerList
            border.color: "black"
            anchors.top: o_RectTcpIpEdit.bottom
            anchors.left: o_RectConnectList.right
            anchors.bottom: parent.bottom
            width: 400
            ListView{
                id:o_ConnectStatusView
                interactive: false
                anchors.fill: parent
                model:  o_Control
                delegate: Rectangle{
                    anchors.left: parent.left
                    height: 20
                    width: 100
                    Text {
                        id: o_textContrName
                        anchors.left: parent.left
                        width: parent.width/4*3
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        text: name


                    }
                    Text{
                        id: o_textContStatus
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: 20
                        text: status
                    }

                }
            }
        }
        Button
        {
            anchors.leftMargin: 20
            id: o_TcpAddButton
            anchors.top: parent.top
            anchors.left: o_RectControllerList.right
            height: 30
            width: 100
            text:"Tcp"
            onClicked: {
                GUIRelate.si_check()
            }
        }
        Button
        {
            anchors.leftMargin: 20
            anchors.topMargin: 20
            id: o_SystemBlocked
            anchors.top: o_TcpAddButton.bottom
            anchors.left:  o_RectControllerList.right
            height: 30
            width: 100
            text: "sys"
            onClicked: {
                ts_Change();
                // GUIRelate.sl_AddItemConnectList(o_TcpIpEdit.text)
            }
        }
        Rectangle
        {
            anchors.leftMargin: 20
            anchors.topMargin: 20
            id: o_RectErrorSystemList
            anchors.top: o_SystemBlocked.bottom
            anchors.left: o_RectControllerList.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 200
            border.color: "black"

            ListView{
                id:o_alarmView
                interactive: false
                anchors.fill: parent
                model:  o_AlarmModel
                delegate:


                    Rectangle{
                    anchors.left: parent.left
                    width: parent.width
                    height: 25

                    TextArea {
                        id: p_AlarmId_control
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: parent.width/16
                        background: Rectangle{ border.color: "black"; anchors.fill: parent }
                        // text: Id_control //o_AlarmModel.data(o_AlarmModel.index(index,0),model.ip)
                    }
                    TextArea {
                        id: p_AlarmAlarm
                        anchors.left: p_AlarmId_control.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: parent.width/16*7
                        text: Alarm
                    }
                    TextArea {
                        id: p_AlarmTime
                        anchors.left: p_AlarmAlarm.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.right: p_AlarmStatus.left
                        text: Time

                    }
                    TextArea {
                        id: p_AlarmStatus
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: parent.height/2 -height/2
                        height: parent.height/16
                        width: height
                        // text: status
                    }
                }
            }
        }
    }

    //                    MouseArea {
    //                        anchors.fill: parent
    //                        onDoubleClicked: {

    //                        }
}
