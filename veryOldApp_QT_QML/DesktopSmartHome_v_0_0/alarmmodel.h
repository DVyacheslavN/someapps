#ifndef ALARMMODEL_H
#define ALARMMODEL_H
#include <abstractmodelqml.h>
#include <QMetaEnum>
#include <alarmabstract.h>
class AlarmModel : public  AlarmAbstract
{
    Q_OBJECT
public:

   // virtual void  m_roleSetFromEnum();
    AlarmModel();
//    enum my_roles {
//        id=Qt::UserRole+1,
//        Alarm,
//        Time,
//        status
//    };
// Q_ENUM(my_roles)
private:


public slots:
    void sl_ClearErrors();
    void m_AppEndRow(QList<QString> data);


    virtual void  sl_AddAlarm(QList<QString> str);
    virtual void  sl_DeleteItem(int position);
    virtual void  sl_Confirm(int position);
};

#endif // ALARMMODEL_H
