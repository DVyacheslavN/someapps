#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <sguiqml.h>
#include <sitemsmodel.h>
#include <modbuscommunication.h>
#include <stcp.h>
#include <sevetn.h>
#include <portlistmodel.h>
#include <tcpsocketmodel.h>
#include <modbusdataquerymodel.h>
#include <modbusdatacrontrollermodel.h>

int main (int argc, char *argv[])
{

    // const bool ld=true;

    //std::enable_if<ld,QMetaObject::Connection>::type connedct(ld) ;
    QGuiApplication app(argc, argv);
    auto *o_ModbusComuniction= new ModbusCommunication;
    auto *o_Gui=new SGuiQml;
    SItemsModel *o_ItemsModel= new SItemsModel();
    STCP *o_TCP=new STCP;
    SEvetn *o_Event=new SEvetn;
    SerialConnect *o_Serial=new SerialConnect;
    PortListModel *o_PortListModel=o_ModbusComuniction->v_PortModel;
    //ModbusDataModel *model=o_ModbusComuniction->v_DataModel;
    ModbusDataQueryModel *o_ModbusQueryModel= o_ModbusComuniction->v_QueryModel;
    TcpSocketModel *o_TcpSocketModel=new TcpSocketModel;
    ModbusDataModel::m_data=o_ModbusComuniction->v_AnswerModel;

    o_TCP->m_GetAllBack=&ModbusDataModel::m_getAllFunc;
    o_Gui->o_Context->setContextProperty("mod_serialList",o_Serial->m_findSerialPort());
    o_Gui->o_Context->setContextProperty("o_ItemsModel",o_ItemsModel);
    o_Gui->o_Context->setContextProperty("o_PortListModel",o_PortListModel);
    o_Gui->o_Context->setContextProperty("o_ClientList",o_TcpSocketModel);
    o_Gui->o_Context->setContextProperty("o_QueryList",o_ModbusQueryModel);
    o_Gui->o_Context->setContextProperty("o_DataControllerModel",o_ModbusComuniction->v_DataControllerModel);
    o_Gui->o_Context->setContextProperty("o_AnsverModel",o_ModbusComuniction->v_AnswerModel);
    o_Gui->sl_WindowQmlCreate();

//    QModbusDataUnit *dataUint=new QModbusDataUnit;
//    QModbusReply *repl=new QModbusReply(QModbusReply::Common,5);
//    dataUint->setRegisterType(QModbusDataUnit::Coils);
//    dataUint->setStartAddress(10);
//    dataUint->setValueCount(3);
//    dataUint->setValue(0,1);
//    dataUint->setValue(1,11);
//    dataUint->setValue(2,22);
//    repl->setResult(*dataUint);
//    ModbusDataModel *o_DataMod=new ModbusDataModel;
//    o_DataMod->m_AddId(5);
//    o_DataMod->m_AddId(3);
//    o_DataMod->m_AddData(3,repl);

//    qDebug () << o_DataMod->rowCount(QModelIndex());
//    qDebug () << o_DataMod->data(o_DataMod->index(3),ModbusDataModel::status);
//    ModbusDataCrontrollerModel control(10,10);

    //std::enable_if
    QObject::connect(o_Gui,SIGNAL(s_SignalServer(int)),o_TCP,SLOT(m_SignalServer(int)));
    QObject::connect(o_Event,SIGNAL(si_ShowEvent(QString,Et::EventT)),o_Gui,SLOT(sl_GetAlarm(QString,Et::EventT)));
    QObject::connect(o_TCP,SIGNAL(s_EventTCP(QString,Et::EventT)),o_Event,SLOT(sl_addEvent(QString,Et::EventT)));
    //QObject::connect(o_TCP,SIGNAL(s_created()),o_ModbusComuniction->v_AnswerModel,SLOT(m_formatModels()));
    QObject::connect(o_ModbusComuniction->v_AnswerModel,SIGNAL(sendAll(QByteArray)),o_TCP,SLOT(m_SendData(QByteArray)));
    QObject::connect(o_TCP,SIGNAL(s_Status(bool)),o_Gui,SLOT(setStatusServer(bool)));
    QObject::connect(o_Gui, &SGuiQml::s_OperPort,o_ModbusComuniction,&ModbusCommunication::m_OpenPort);
    QObject::connect(o_Serial,&SerialConnect::s_EventSerial,o_Event,&SEvetn::sl_addEvent);
    QObject::connect(o_Gui,&SGuiQml::s_StartStopQueryTimer,o_ModbusComuniction,&ModbusCommunication::m_TimerStopStart);
    //QObject::connect(o_Serial,&SerialConnect::s_AddPort,o_Controller,&ControllerModel::m_AppEndRow);
    QObject::connect(o_TCP,&STCP::s_TcpSocketSend,o_TcpSocketModel,&TcpSocketModel::m_TcpSocketAppend);
    QObject::connect(o_TCP,&STCP::m_WriteQuery,o_ModbusComuniction,&ModbusCommunication::m_WriteQueryTCP);
    //QObject::connect(o_Gui,&SGuiQml::s_DeleteClient,o_TcpSocketModel,&TcpSocketModel::m_DeleteClietn);
    //  auto dd=  <void (STCP::*)(QTcpSocket *)> (&STCP::s_TcpSocketSend);
    // template<class T>
    // qDebug()  << dd << "dddd";
    //            connect(writeModel, &WriteRegisterModel::updateViewport, ui->writeValueTable->viewport(),
    //                static_cast<void (QWidget::*)()>(&QWidget::update));

    return app.exec();
}


