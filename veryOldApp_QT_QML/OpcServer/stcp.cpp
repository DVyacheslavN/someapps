#include "stcp.h"
#include <QDataStream>
#include <sitemsmodel.h>
#include <QHash>
STCP::STCP()
{
    v_TcpServer=new QTcpServer;

}

void STCP::m_SignalServer(int nPort)
{
    if (!v_TcpServer->isListening()){
        if( !v_TcpServer->listen(QHostAddress::Any, nPort))
        {
            s_EventTCP("can't create a server",Et::Alarm);
        }
        else {
            connect(v_TcpServer,SIGNAL(newConnection()),this,SLOT(m_NewConnect()));
            emit s_EventTCP("StartServer",Et::Event);
            emit s_Status(true);
        }
    }
    else {
        m_stopServer();
        emit s_EventTCP("StopServer",Et::Event);
        emit s_Status(false);
    }

}

void STCP::m_NewConnect()
{
    v_TcpSocket= v_TcpServer->nextPendingConnection();
    if (!v_TcpSocket->isValid())
    {
        s_EventTCP("TcpSocket the null",Et::Alarm);
        return;
    }
    tcp.setDevice(v_TcpSocket);

    connect(v_TcpSocket,&QTcpSocket::readyRead,this,&STCP::m_GetData);
    //connect(v_TcpSocket,SIGNAL(disconnected()),v_TcpSocket,SLOT(deleteLater()));
    s_EventTCP("NewConnect",Et::Event);

   // emit s_created();
    emit s_TcpSocketSend(v_TcpSocket);
}




void STCP::m_stopServer()
{
    v_TcpServer->close();
}

bool STCP::m_SendData(QByteArray byte)
{
    if (GlobalAbstractTCP::m_SendData(byte))
    emit s_EventTCP("TcpSocket it's closed",Et::Alarm);
    emit s_EventTCP("Data Send",Et::Event);
    return true;
}


void STCP::m_ParseGetData(QByteArray data)
{
    QDataStream l_Stream(data);
    quint16 type;
    l_Stream >> type;
    switch (type) {
    case (int)TCPQandR::QAllData:
        qDebug () << "QueryAllData";
        //qDebug() <<  "get Query All" << (m_GetAllBack()).keys();
        m_SendData(m_TypeToByte((int)TCPQandR::AAllData,m_GetAllBack()));
        break;
    case (int)TCPQandR::QWriteReg:
    {
        qDebug () << "QWriteReg";
        //data.mid()
        emit m_WriteQuery(data.mid(sizeof(type)));
        qDebug ()  << "WriteBuf;";
    }
        break;
    default:
        qDebug () << "NO Query" << type;
        break;
    }
}

