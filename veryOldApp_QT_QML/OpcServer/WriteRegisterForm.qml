import QtQuick 2.0
import QtQuick.Controls 2.2
Item {

    // [type,typeCon,conAdr,stAdr,numb]
    property var settings: [0,0,0,0,0]

    property var previousY
    property var previousX
    MouseArea{
        anchors.fill: parent
        onPressed: {
            // Запоминаем позицию по оси Y
            previousY = mouseY
            previousX = mouseX
//                    parent.setY(parent.y + 100)
//                    parent.setHeight(parent.height - 100)
        }
        onMouseYChanged: {
            //console.log(mouseY)
            var dy = mouseY - previousY
            var dx = mouseX - previousX
            parent.y= (parent.y + dy)
            parent.x =(parent.x + dx)
//           // parent.setHeight(parent.height - dy)
            parent.anchors.horizontalCenter=undefined
            parent.anchors.top=undefined
            parent.anchors.topMargin=undefined
        }

    }

    MouseArea{
        id: o_mouseA
        anchors.right: parent.right
        anchors.bottom: parent.top
        anchors.bottomMargin:  -3
        height: 14
        width: height
        onPressed: {
            // Запоминаем позицию по оси Y
            previousY = mouseY
            previousX = mouseX
            //                    parent.setY(parent.y + 100)
            //                    parent.setHeight(parent.height - 100)
        }
        onMouseYChanged: {
            //console.log(mouseY)
            var dy = mouseY - previousY
            var dx = mouseX - previousX
            parent.y= (parent.y + dy)
            parent.x =(parent.x + dx)
            //           // parent.setHeight(parent.height - dy)
            parent.anchors.horizontalCenter=undefined
            parent.anchors.top=undefined
            parent.anchors.topMargin=undefined
        }

    }
    Rectangle
    {
        anchors.right: parent.right
        anchors.bottom: parent.top
        anchors.bottomMargin:  -3
        z:4
        height: o_mouseA.height
        radius: height/2
        width: height

    }
    Rectangle{
    anchors.fill: parent
    border.color: "#c5c4cc"
    border.width: 2
    Rectangle{
        anchors.fill: parent
        color: "#cacaca"
        anchors.margins: 2

        Column{
            anchors.fill: parent


            Row{
                anchors.left: parent.left
                height: 30
                width: parent.width
                Rectangle{
                    width: parent.width/6
                    height: parent.height
                    border.color: "black"
                    color: l_portName.currentText !="Invalid" ? "#bcdeb1" : "#bdbdbd"
                    Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                    text: "PortName"
                }
                }
                Rectangle{
                    border.color: "black"
                    width: parent.width/6
                    height: parent.height
                    color: l_Typereg.currentText !="Invalid" ? "#bcdeb1" : "#bdbdbd"
                Text {
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                    anchors.fill: parent
                    text: "TypeReg"
                }
                }
                Rectangle{
                    border.color: "black"
                    width: parent.width/6
                    height: parent.height
                    color: l_addresContrller.text !="0" &&
                           l_addresContrller.text !="" ? "#bcdeb1" : "#bdbdbd"
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                    text: "AddresContrller"
                }
                }
                Rectangle{
                    border.color: "black"
                    width: parent.width/6
                    height: parent.height
                    color: l_AddrsRegister.text !="0" &&
                           l_AddrsRegister.text !="" ? "#bcdeb1" : "#bdbdbd"
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                    text: "AddrsRegister"
                }
                }
                Rectangle{
                    border.color: "black"
                    width: parent.width/6
                    height: parent.height
                    color: l_SetData.text !="" ? "#bcdeb1" : "#bdbdbd"
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                    text: "SetData"
                }
                }
                Button
                {
                    background: Rectangle {color: "#fcf9b3"
                        border.color: "black"
                        border.width: 1 }
                    height: parent.height
                    width: parent.width/6
                    text: "Cancel"
                    onClicked: parent.parent.parent.parent.destroy()
                }
            }

            Row{
                anchors.left: parent.left
                height: 30
                width: parent.width

                ComboBox {
                    background: Rectangle {border.color: "black"}
                    height: parent.height
                    id: l_portName
                    width: parent.width/6
                    model: o_PortListModel.PortAdded
                    currentIndex: settings[1]
                }
                ComboBox {
                    background: Rectangle{border.color: "black"}
                    height: parent.height
                    id: l_Typereg
                    width: parent.width/6

                    model: o_QueryList.m_QueryListGet()
                    currentIndex: settings[0]
                }
                TextField {
                    height: parent.height
                    id: l_addresContrller
                    width: parent.width/6
                    text: settings[2]
                }
                TextField {
                    height: parent.height
                    id: l_AddrsRegister
                    width: parent.width/6
                    text: settings[3]
                }

                TextField {
                    height: parent.height
                    id: l_SetData
                    width: parent.width/6
                    text: settings[4]
                }
                Button
                {
                    background:  Rectangle {color: enabled ? "#bcdeb1" : "#fcc3b3"
                        border.color: "black"}
                    height: parent.height
                    width: parent.width/6
                    text: "SendQuery"
                    enabled: (l_portName.currentText && (l_portName.currentText != "Invalid")  && l_Typereg.currentIndex &&
                              (l_addresContrller.text != "0"  && l_addresContrller.text != "" ) &&
                             (l_AddrsRegister.text != "0" && l_AddrsRegister.text != "") && l_SetData.text)
                    onClicked: {
                        o_QueryList.m_WriteRegisters([l_portName.currentIndex,l_Typereg.currentIndex,l_addresContrller.text,
                                                     l_AddrsRegister.text,l_SetData.text])
                    }
                }
            }
        }
    }
}
}
