#include "sguiqml.h"
#include <QSerialPort>
#include <QDebug>
#include <modbusdatamodel.h>
SGuiQml::SGuiQml()
{

    int parity = QSerialPort::NoParity;
    int baud = QSerialPort::Baud19200;
    int dataBits = QSerialPort::Data8;
    int stopBits = QSerialPort::OneStop;
    int responseTime = 1000;
    int numberOfRetries = 3;
    ModbusDataModel *o_DataModbus =new ModbusDataModel;

    v_Settings << parity <<baud <<dataBits <<stopBits <<responseTime << numberOfRetries;
    o_Engine= new QQmlApplicationEngine;
    o_Context= o_Engine->rootContext();
    o_Context->setContextProperty("GUIRelate",this);
    o_Context->setContextProperty("o_DataModbus",o_DataModbus);




}

void SGuiQml::sl_WindowQmlCreate()
{
    o_Engine->load(QUrl(QLatin1String("qrc:/main.qml")));

    QObject *obj = sl_ObjecQmlRequested("root/vertBar");
    connect(obj,SIGNAL(on_SignalServer(int )),this,SIGNAL(s_SignalServer(int)));
    connect(obj,SIGNAL(on_DeleteController(int )),this,SIGNAL(s_DeleteController(int)));
    obj = sl_ObjecQmlRequested("root/ocenterIn");
    connect(obj,SIGNAL(on_StartStopQueryTimer (bool)),this,SIGNAL(s_StartStopQueryTimer(bool)));
//    obj= sl_ObjecQmlRequested("root/horBar");
//    connect(obj,SIGNAL(on_DeleteClient(int)),this,SIGNAL(s_DeleteClient(int)));
//    obj = sl_ObjecQmlRequested("root/horBar");

}

void SGuiQml::sl_GetAlarm(QString str, Et::EventT type)
{
    if (type== Et::Alarm) emit si_ShowMessageAlarm(str);
    emit si_BarMessage(str);
}

void SGuiQml::setStatusServer(bool stateser)
{
 v_SerRedy= stateser;
 emit StatusServerChanged();
}

bool SGuiQml::StatusServer()
{
    return v_SerRedy;
}


void SGuiQml::setSettingList(int i, int Data)
{
    v_Settings[i]=Data;
    //qDebug () << v_Settings.at(i) << "DATA";
}

void SGuiQml::m_AddQuery(int type, int startAdres, int nubmer)
{
}


QObject *SGuiQml::sl_ObjecQmlRequested(QString Path)
{
    QObject * bufReturn=nullptr;
    if (1) {
        QList<QObject*> root=o_Engine->rootObjects();

        QRegExp reg ("[a-zA-Z0-9]{1,9}");
        int iterator=0;
        bufReturn=root[0];
        while (((iterator=reg.indexIn(Path,iterator))!=-1) && bufReturn!=nullptr) {
            iterator+=reg.matchedLength();
            bufReturn=bufReturn->findChild<QObject*>(reg.cap(0));
        }
       // emit si_ObjecQmlSending(bufReturn);
    }
  //  else qDebug () << "Isn't created EngineObject";
    return bufReturn;
}
