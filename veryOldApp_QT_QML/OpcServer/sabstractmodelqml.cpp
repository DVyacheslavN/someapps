#include "sabstractmodelqml.h"
#include <QDebug>
SAbstractModelQml::SAbstractModelQml()
{
m_roleSetFromEnum();
}
int SAbstractModelQml::rowCount(const QModelIndex &parent) const
{
    return v_AbstractModelQML.size();
}

int SAbstractModelQml::columnCount(const QModelIndex &parent) const
{
    return v_AbstractModelQML.at(0).size();
}

QVariant SAbstractModelQml::data(const QModelIndex &index, int role) const
{

    return  v_AbstractModelQML.at(index.row()).value(role);
}

bool SAbstractModelQml::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid()
            && value.isValid()
            && role)
    {
        v_AbstractModelQML[index.row()][role]=value;
        dataChanged(index,index);
         endResetModel();
        return true;
    }
    return false;
}

Qt::ItemFlags SAbstractModelQml::flags(const QModelIndex &index) const
{

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

QHash<int, QByteArray> SAbstractModelQml::roleNames() const
{
    QHash <int, QByteArray> roles=QAbstractTableModel::roleNames();
    int x=0;
    for (int i=0 ; (x=v_rolseEnumStr.value(i))>0 ;i++) {
        roles[x]=v_rolseEnumStr.key(i);
    }

    return roles;
}

QModelIndex SAbstractModelQml::index(int row, int column, const QModelIndex &parent) const
{
    return QAbstractTableModel::createIndex(row,column);
}

void SAbstractModelQml::m_roleSetFromEnum()
{
    v_rolseEnumStr=QMetaEnum::fromType<ALLrole>();
}

void SAbstractModelQml::removeRow(int row, const QModelIndex &parent)
{

    v_AbstractModelQML.removeAt(row);
//    QHash<int, QHash<int, QVariant>>::iterator it= v_AbstractModelQML.begin();
//    for (int i=0;i<row && it!=v_AbstractModelQML.end();i++) it++;
//   qDebug () <<  it.key();
//   // v_AbstractModelQML.erase(it);
//    v_AbstractModelQML.remove(it.key());

//    qDebug () << row << "delete row";
//     qDebug () << v_AbstractModelQML.keys() << "to keys";
//      qDebug () << v_AbstractModelQML[row][258];
//   v_AbstractModelQML.remove(row);
//   qDebug ()<< v_AbstractModelQML.keys()<< "do keys";
//   qDebug () << v_AbstractModelQML.value(row);

    endResetModel();
}

QVariant SAbstractModelQml::m_data(int row,int role)
{
//    QHash<int, QHash<int, QVariant>>::iterator it= v_AbstractModelQML.begin();
//    for (int i=0;i<row && it!=v_AbstractModelQML.end();i++) it++;
    //return it.value().value(role);
}

int SAbstractModelQml::v_siseRow()
{
    return v_AbstractModelQML.size();
}

void SAbstractModelQml::sl_WriteAll(QList<QHash<int, QVariant> > * data)
{
    qDebug() << v_AbstractModelQML.size();
    v_AbstractModelQML.clear();
    v_AbstractModelQML << (*data);
    endResetModel();
    qDebug() << "write model";
    qDebug() << v_AbstractModelQML.size();
    emit v_siseRowChanged();
}
