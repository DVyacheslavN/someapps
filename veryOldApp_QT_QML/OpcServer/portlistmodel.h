#ifndef PORTLISTMODEL_H
#define PORTLISTMODEL_H
#include <QAbstractListModel>
#include <serialconnect.h>
#include <QMetaEnum>
#include <QSerialPort>
#include <QDebug>
class PortListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QStringList PortNames READ PortNames NOTIFY PortNameChanged)
    Q_PROPERTY(QStringList PortAdded READ PortAdded NOTIFY PortAddedChanged)


public:


    //    int parity = QSerialPort::NoParity;
    //    int baud = QSerialPort::Baud19200;
    //    int dataBits = QSerialPort::Data8;
    //    int stopBits = QSerialPort::OneStop;
    //    int responseTime = 1000;
    //    int numberOfRetries = 3;

    PortListModel();
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;
    QHash<int ,QByteArray> roleNames() const override;
    QMetaEnum my_enum;
    QMetaEnum v_Parity;
    QMetaEnum v_Baud;
    QMetaEnum v_DataBits;
    QMetaEnum v_StopBits;

    enum my_role {
        id=Qt::UserRole+1,
        names,
        status
    };
    Q_ENUM(my_role)
    void m_Query(int PortId, int Adres, QModbusDataUnit query, bool Write=false);
    QList<int> m_GetSettingFormIndex(QList<int> indexes);
    void m_SaveSetting(QList<int> setting ){v_settings.append(setting);}
    Q_INVOKABLE QList<int> m_GetSettingToView(int indexs){return v_settings.at(indexs);}
    Q_INVOKABLE void m_SetSettingToModel(int indexs, QList<int> arraySetting)
    {
        v_settings.replace(indexs,arraySetting);
        v_DataList.at(indexs)->m_SetSettingPort(m_GetSettingFormIndex(arraySetting));
    }
private:
    QList <SerialConnect*> v_DataList;
    QList <QList<int>> v_settings;
public slots:
    void m_ConnectDevice (int index) { v_DataList.at(index)->m_GetStatus() ?
                    v_DataList.at(index)->m_ConectDisDevice(false) :
                    v_DataList.at(index)->m_ConectDisDevice(true);}
    void m_DeleteConndected(int index) {if (v_DataList.at(index)->m_GetStatus())  v_DataList.at(index)->m_ConectDisDevice(false);
                                       SerialConnect*buf= v_DataList.at(index);
                                       v_DataList.removeAt(index); delete buf;
                                       endResetModel(); v_settings.removeAt(index);}
    Q_INVOKABLE void m_AppEndRow(SerialConnect * serialport);
    void m_statCnahge(QString Name , bool Staus);
    QStringList PortNames ();
    QStringList prop_Parity() {QStringList buf; v_Parity=QMetaEnum::fromType<QSerialPort::Parity>();
                              for (int i=0; v_Parity.value(i)>-1 ; i++){ buf.append(v_Parity.key(i));} return buf;}
    QStringList prop_Baud(){QStringList buf;  v_Baud=QMetaEnum::fromType<QSerialPort::BaudRate>();
                            for (int i=0; v_Baud.value(i)>-1 ; i++) {buf.append(v_Baud.key(i));}  return buf;}
    QStringList prop_Data(){QStringList buf; v_DataBits=QMetaEnum::fromType<QSerialPort::DataBits>();
                            for (int i=0; v_DataBits.value(i)>-1 ; i++) {buf.append(v_DataBits.key(i));}  return buf;}
    QStringList prop_Stop(){QStringList buf;  v_StopBits=QMetaEnum::fromType<QSerialPort::StopBits>() ;
                            for (int i=0; v_StopBits.value(i)>-1 ; i++) {buf.append(v_StopBits.key(i));}  return buf;}

signals:
    void PortNameChanged();
    void PortAddedChanged();

private slots:
    void m_serialStateChanged();
    QStringList PortAdded(){QStringList buf;
                            for (auto dt:v_DataList) { buf.append( dt->m_getName().toString());}
                                                buf.append("Invalid"); return buf;  }
};

#endif // CONTROLLERMODEL_H
