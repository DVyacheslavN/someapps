import QtQuick 2.9
import QtQuick.Controls 2.2
Item {
    height: 110
    property var previousY
    property var previousX
    MouseArea{

        anchors.fill: parent
        onPressed: {
            // Запоминаем позицию по оси Y
            previousY = mouseY
            previousX = mouseX
            //                    parent.setY(parent.y + 100)
            //                    parent.setHeight(parent.height - 100)
        }
        onMouseYChanged: {
            //console.log(mouseY)
            var dy = mouseY - previousY
            var dx = mouseX - previousX
            parent.y= (parent.y + dy)
            parent.x =(parent.x + dx)
            //           // parent.setHeight(parent.height - dy)
            parent.anchors.horizontalCenter=undefined
            parent.anchors.top=undefined
            parent.anchors.topMargin=undefined
        }

    }

    MouseArea{
        id: o_mouseA
        anchors.right: parent.right
        anchors.bottom: parent.top
        anchors.bottomMargin:  -3
        height: 14
        width: height
        onPressed: {
            // Запоминаем позицию по оси Y
            previousY = mouseY
            previousX = mouseX
            //                    parent.setY(parent.y + 100)
            //                    parent.setHeight(parent.height - 100)
        }
        onMouseYChanged: {
            //console.log(mouseY)
            var dy = mouseY - previousY
            var dx = mouseX - previousX
            parent.y= (parent.y + dy)
            parent.x =(parent.x + dx)
            //           // parent.setHeight(parent.height - dy)
            parent.anchors.horizontalCenter=undefined
            parent.anchors.top=undefined
            parent.anchors.topMargin=undefined
        }

    }
    Rectangle
    {
        anchors.right: parent.right
        anchors.bottom: parent.top
        anchors.bottomMargin:  -3
        z:4
        height: o_mouseA.height
        radius: height/2
        width: height

    }

        Rectangle{
            anchors.fill: parent
            color: "#366191"
            radius: 1
            ComboBox{
                id: o_TyepQuery
                anchors.left: parent.left
                anchors.top: parent.top
                height: 30
                anchors.margins: 5
                width:  parent.width/2-7.5
                model: o_QueryList.m_QueryListGet()   //["Coils","Discret Input","Input Registers","Holding Registers"]
                currentIndex: 1
            }
            ComboBox
            {
                id: o_TypeConnectPortId
                anchors.left: o_TyepQuery.right
                anchors.top: parent.top
                anchors.margins: 5
                height: 30
                width: parent.width/2-7.5
                model: o_PortListModel.PortAdded
            }

            Rectangle {
                id: o_rowEdit
                anchors.top: o_TyepQuery.bottom
                anchors.left:  parent.left
                anchors.right:   parent.right
                anchors.margins: 5
                height: 30
                width: parent.width
                Row {
                    id: o_rowRow
                    anchors{
                        left: parent.left
                        top: parent.top
                        bottom: parent.bottom
                    }



                    Text {
                        height: 30
                        text: qsTr("Addres Controller:")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment:  Text.AlignVCenter
                    }
                    TextField {
                        id: o_AddresClient
                        height: 30

                        width: parent.width/10
                        text: "1"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    Text {
                        height: 30
                        text: qsTr("Addres Registr:")
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment:  Text.AlignVCenter
                    }
                    TextField{
                        id: o_AddresQuery

                        height: 30

                        width: parent.width/10
                        text: "5"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    Text {
                        height: 30
                        text: qsTr("Number Registr:")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment:  Text.AlignVCenter
                    }

                }
                TextField{
                    id: o_NumberQuery
                    anchors.left: o_rowRow.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    height: 30

                    width: parent.width/10
                    text:"2"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Button{
                anchors.left: parent.left
                anchors.top: o_rowEdit.bottom
                height: 30
                anchors.margins: 5
                width: parent.width/2-anchors.margins*2
                text: "cancel"
                onClicked:
                {
                    parent.parent.destroy()
                }
            }
            Button{
                anchors.right: parent.right
                anchors.top: o_rowEdit.bottom
                height: 30
                anchors.margins: 5
                width: parent.width/2-anchors.margins*2
                text: "Add Query"
                enabled: (o_TyepQuery.currentIndex && o_TypeConnectPortId.currentText && o_AddresClient.text && o_AddresQuery.text && o_NumberQuery.text)? true : false
                onClicked:
                {
                    o_QueryView.l_selectItem=false
                    var buf=[] ;
                    buf[0]=o_TyepQuery.currentIndex;
                    buf[1]=o_TypeConnectPortId.currentIndex;
                    buf[2]=o_AddresClient.text;
                    buf[3]=o_AddresQuery.text;
                    buf[4]=o_NumberQuery.text;

                    o_QueryList.m_AddQuery(buf);
                    /*o_TyepQuery.currentIndex+1,
                                       o_TypeConnectPortId.currentIndex,
                                       o_AddresClient.text,
                                       o_AddresQuery.text,
                                       o_NumberQuery.text)*/
                    //(o_TyepQuery.currentIndex+1,o_AddresQuery.text,o_NumberQuery.text)
                    // GUIRelate.m_AddQuery( o_AddresQuery.text,o_NumberQuery.text)
                }
            }

        }
    }
