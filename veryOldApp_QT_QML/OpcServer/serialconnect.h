#ifndef SERIALCONNECT_H
#define SERIALCONNECT_H
#include <QSerialPort>
#include <QModbusDevice>
#include <QModbusClient>
#include <QModbusRtuSerialMaster>
#include <QObject>
#include <QVariant>
#include <enumproj.h>

class SerialConnect : public QObject
{
    Q_OBJECT
public:
    explicit SerialConnect(int l_idConnect=0,QObject *parent = nullptr);

    QStringList v_serialPortList;
    QStringList m_findSerialPort();
    QVariant m_getName(){return (modbusDevice->connectionParameter(QModbusDevice::SerialPortNameParameter));}
    bool m_GetStatus() {return modbusDevice->state()==QModbusDevice::ConnectedState;}
    void m_ConectDisDevice (bool Conct_Disect) { Conct_Disect ?  (void)modbusDevice->connectDevice() : (void)modbusDevice->disconnectDevice();}
signals:
    void s_EventSerial(QString ,Et::EventT);
    void s_PortAdded(QString name, bool status);
    void s_StausChanged();
   // void s_Ansver(QString port,int Adr, QHash<int, QVariant>  *data);
    void s_Ansver(int l_idConnect, QModbusReply *data);
public slots:

    bool m_OpenPort(QString name,QList <int> setting);
    void m_SetSettingPort(QList<int> setting);
    void m_Query (int ControllerAdres, QModbusDataUnit s_Query, bool Write=false);
    //void m_set
private:
    int v_idConnect;
    QModbusRtuSerialMaster *modbusDevice;
private slots:
    void m_stateCnahged(QModbusDevice::State state);
    void ts_TimeSendQuery();
    void m_ReadyRead();

};

#endif // SERIALCONNECT_H
