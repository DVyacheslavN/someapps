#ifndef STCP_H
#define STCP_H
#include <sabstracttcp.h>
#include <QVariant>
#include <../tcpconnectingprograms.h>
#include <enumproj.h>
#include <QTimer>
class STCP : public SAbstractTCP ,public GlobalAbstractTCP
{
    Q_OBJECT
public:
    STCP();
    void m_stopServer();

    QHash<QList<int>, QList<QList<int> > > (*m_GetAllBack)(); // CallBack
private:
    QTimer time;
    void m_SignalServer(int nPort=2323);
    void m_NewConnect();

    virtual void m_ParseGetData (QByteArray data) override;
    QVariant v_GetBuf;
    QList<QHash<int ,QVariant>> v_buf;
private slots:

    virtual void m_GetData() override {GlobalAbstractTCP::m_GetData();}
    public slots:
    virtual bool m_SendData(QByteArray byte) ;
signals:
    void s_Status(bool staus);
    void s_TcpSocketSend(QTcpSocket *Socket);
    void m_WriteQuery(QByteArray data) ;
};

#endif // STCP_H
