#include "portlistmodel.h"
#include "QDebug"
#include <QSerialPortInfo>
PortListModel::PortListModel()
{
my_enum=QMetaEnum::fromType<my_role>();
}

QVariant PortListModel::data(const QModelIndex &index, int role) const
{
     // qDebug () << v_DataList.at(index.row())->m_getName() << "name";
    if (role==names) return v_DataList.at(index.row())->m_getName();
    if (role==status) return v_DataList.at(index.row())->m_GetStatus();
    return QVariant();

}

int PortListModel::rowCount(const QModelIndex &parent) const
{
return v_DataList.size();
}

QHash<int, QByteArray> PortListModel::roleNames() const
{
    QHash <int ,QByteArray> b_buf;
    int x=0;
    for (int i=0 ; (x=my_enum.value(i))>0 ;i++) {
        b_buf[x]=my_enum.key(i);
    }

    qDebug () << b_buf << "buf";
    return b_buf;
}

void PortListModel::m_Query(int PortId,int Adres, QModbusDataUnit query,bool Write)
{
   // qDebug () << "PortId:" <<PortId <<"adres :" << Adres << "startadres :" <<  query.startAddress();
   if( v_DataList.size()>PortId)
       v_DataList.at(PortId)->m_Query(Adres,query,Write);
   else qDebug () << "error QueryTimer";
}

QList<int> PortListModel::m_GetSettingFormIndex(QList<int> indexes) /* parity, baud ,dataBits, stopBits, <-indexes
                                                                      and responseTime, numberOfRetries <-numbers
                                                                      */
{
        QList<int> buf;

        buf.append(v_Parity.value(indexes.at(0)));
        buf.append(v_Baud.value(indexes.at(1)));
        buf.append(v_DataBits.value(indexes.at(2)));
        buf.append(v_StopBits.value(indexes.at(3)));
        buf.append(indexes.at(4));
        buf.append(indexes.at(5));
        return buf;

}



void PortListModel::m_AppEndRow(SerialConnect * serialport)
{

    v_DataList.append(serialport);
    connect(serialport,&SerialConnect::s_StausChanged,this,&PortListModel::m_serialStateChanged);
    emit PortAddedChanged();
    endResetModel();
}

void PortListModel::m_statCnahge(QString Name, bool Staus)
{

//    for (auto &find : v_AbstractModelQML )
//    {
//        if (!find.value(name).toString().compare(Name))
//        {
//            find[status]=Staus;
//            endResetModel();
//        }
//    }
}

QStringList PortListModel::PortNames()
{
    QStringList serialPortList;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
       serialPortList.append(info.portName());

    return serialPortList;
}

void PortListModel::m_serialStateChanged()
{
    qDebug () << "stateCnage port list";
    endResetModel();
}
