import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
Window {
    visible: true
    width:  Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight
    title: qsTr("Hello World")



    Item {
        id: o_Root
        objectName: "root"
        anchors.fill: parent


        Connections{
            target: GUIRelate
            onSi_ShowMessageAlarm:
            {o_MessageAlarm.visible=true
                o_MessageText.text= str
            }
        }

        Rectangle{
            id: o_MessageAlarm
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: 30
            z: 1
            border.color: "black"
            width: 300
            visible: false
            Text {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                id: o_MessageText
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }
            Button
            {
                anchors.verticalCenter:  parent.verticalCenter
                height: 20
                width: height
                anchors.right: parent.right
                anchors.rightMargin:   parent.height/2 -height/2
                onClicked: parent.visible=false
            }
        }

        Menu1 {
           id: o_menutop
           anchors.top: parent.top
           anchors.left: parent.left
           anchors.right: parent.right
           height: 30
        }

        VerticalItem{
            id: o_Vertic
            objectName: "vertBar"
            anchors.left: parent.left
            anchors.top: o_menutop.bottom
            anchors.bottom: o_Horizont.top
            width:  250
        }
        HorisontalItem{
            id: o_Horizont
            objectName: "horBar"
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 60

        }

        CenterInView {
            id: o_CenterIn
            objectName: "ocenterIn"
            anchors.right: parent.right
            anchors.top: o_menutop.bottom
            anchors.bottom: o_Horizont.top
            anchors.left: o_Vertic.right
        }

//        CotrollerSettingForm{
//            id: o_ControllerSettingForm
//            anchors.centerIn: parent
//           // height: 300
//            z:2
//            width: 450
//            visible: false
//        }

//        AddQueryForm{
//            id: o_AddQueryForm
//            anchors.horizontalCenter: parent.horizontalCenter
//            anchors.top: parent.top
//            anchors.topMargin: 100
//            z:3
//            width: 450

//            visible: false
//        }

    }

}
