#include "modbusdataquerymodel.h"
#include <QHashIterator>
#include <QDebug>
#include <qstringlistmodel.h>
ModbusDataQueryModel::ModbusDataQueryModel()
{
    v_RoleEnum=QMetaEnum::fromType<my_role>();

    //v_DataUnitQuery.prepend(data);
    //    v_valueFilter=new int[4]();
    //    v_valueFilter[2] =-1;
}

int ModbusDataQueryModel::rowCount(const QModelIndex &parent) const
{
    v_DataUnitQuery.size();
}

QHash<int, QByteArray> ModbusDataQueryModel::roleNames() const
{
    QHash <int ,QByteArray> buf;
    int x=0;
    for (int i=0; (x=v_RoleEnum.value(i))>0;i++)
    {
        buf[x]=v_RoleEnum.key(i);
    }
    return buf;
}

QVariant ModbusDataQueryModel::data(const QModelIndex &index, int role) const
{
    //if (index.row() > v_DataUnitQuery.size()) qDebug ()<< "range out";
    //auto v_dataIterator = v_DataUnitQuery.begin();


    //QList<int> v_key= (v_dataIterator+index.row()).key(); //index.row(); /**/
    //if (v_key.size() <2) return QVariant("Null");
    //    auto v_Value = (v_dataIterator+index.row()).value();
    auto v_Buf= v_DataUnitQuery.at(index.row());
    if (role==stAdr)    return QVariant( v_Buf[LstQueryEnum::startS]);
    if (role==numb)     return QVariant( v_Buf[LstQueryEnum::numb]);
    if (role==type)     return QVariant( v_Buf[LstQueryEnum::typeReg]);
    if (role==conAdr)   return QVariant( v_Buf[LstQueryEnum::addresClient]);
    if (role==port){   qDebug() <<v_Buf[LstQueryEnum::PortIdConnect] << "rolePort";
        return QVariant( v_Buf[LstQueryEnum::PortIdConnect]);}

    return QVariant("Null");
}

QModbusDataUnit ModbusDataQueryModel::m_formatDataList(Ltype *data)
{
    QModbusDataUnit buf (static_cast<QModbusDataUnit::RegisterType>(data[LstQueryEnum::typeReg])
            ,data[LstQueryEnum::startS],data[LstQueryEnum::numb]);
    return buf;
}

void ModbusDataQueryModel::m_WriteRegisters(QVariant WriteRegData)
{
//    o_QueryList.m_WriteRegisters([l_portName.currentIndex ,l_Typereg.currentIndex, l_addresContrller.text,
//                                 l_AddrsRegister.text,l_SetData.text])
    Ltype *v_bufDataUnitQuery=new Ltype[5];
    //s_WrieteRegister
    v_bufDataUnitQuery[LstQueryEnum::PortIdConnect]=WriteRegData.toList().at(0).toInt();
    v_bufDataUnitQuery[LstQueryEnum::typeReg]=WriteRegData.toList().at(1).toInt();
    v_bufDataUnitQuery[LstQueryEnum::addresClient]=WriteRegData.toList().at(2).toInt();
    v_bufDataUnitQuery[LstQueryEnum::startS]=WriteRegData.toList().at(3).toInt();
    v_bufDataUnitQuery[LstQueryEnum::numb]=1;
    auto bufQuery=  m_formatDataList(v_bufDataUnitQuery);
    bufQuery.setValue(0,WriteRegData.toList().at(4).toInt());
    emit s_WrieteRegister(bufQuery,v_bufDataUnitQuery[LstQueryEnum::PortIdConnect], v_bufDataUnitQuery[LstQueryEnum::addresClient]);
}

void ModbusDataQueryModel::m_AddQuery(QVariant data)
{
    Ltype *v_bufDataUnitQuery=new Ltype[5];
    qDebug () /*<< typeS << PortIdConnect << addresClient << startS << numbS*/ <<data.toList()  << "Data GEt";
    //     QModbusDataUnit data (static_cast<QModbusDataUnit::RegisterType>(typeS),startS,numbS);
    //     QList<int> set;
    v_bufDataUnitQuery[LstQueryEnum::typeReg]=data.toList().at(0).toInt();
    v_bufDataUnitQuery[LstQueryEnum::PortIdConnect]=data.toList().at(1).toInt();
    v_bufDataUnitQuery[LstQueryEnum::addresClient]=data.toList().at(2).toInt();
    v_bufDataUnitQuery[LstQueryEnum::startS]=data.toList().at(3).toInt();
    v_bufDataUnitQuery[LstQueryEnum::numb]=data.toList().at(4).toInt();
    //  set << v_max <<PortIdConnect << addresClient;

    v_DataUnitQuery.append(v_bufDataUnitQuery);
    //    v_valueFilter[0]=0;
    //    v_valueFilter[1]=0;
    //    v_valueFilter[2]=0;
    //    v_max++;
    endResetModel();
    emit s_QueryListChange(&v_DataUnitQuery);



}

void ModbusDataQueryModel::m_FilterSet(int typeFilter)
{

}

QList<int> ModbusDataQueryModel::m_filter(int row, int role) const
{
    //    int index=0;

    //   // v_valueFilter[0]=255;
    //    //v_valueFilter[1]=buf->at(1);
    //    for (auto &bufer : v_DataUnitQuery.keys())
    //    {
    ////          if (bufer->at(0)<(v_valueFilter[0] && bufer->at(0) > (v_valueFilter[2] ) ))
    ////          {buf=bufer;
    ////              v_valueFilter[0]=bufer->at(0);

    ////          }
    //        if (bufer.at(0)==row) return bufer;
    //    }
    //   // v_valueFilter[2]=buf->at(0);
    // if (buf) return buf;
    return QList<int>();
}


