#include "modbuscommunication.h"
#include <modbusdataquerymodel.h>
#include <QList>
#include <QDebug>
#include <../globalmodel.h>
ModbusCommunication::ModbusCommunication()
{
    v_Timer=new QTimer;
    v_Timer->setInterval(100);
    connect(v_Timer,&QTimer::timeout,this,&ModbusCommunication::m_timeOut);
    //v_serialPortList << new SerialConnect;
    //v_AnswerModel=new ModbusDataModel;
    v_QueryModel=new ModbusDataQueryModel;
    v_PortModel=new PortListModel;
    v_DataControllerModel=new ModbusDataCrontrollerModel();
    // v_DataControllerModel->m_AddData(10,1,1,1);
    connect(v_QueryModel,&ModbusDataQueryModel::s_QueryListChange,this,&ModbusCommunication::m_GetModbusDataUnit);
    connect(v_QueryModel,&ModbusDataQueryModel::s_WrieteRegister,this,&ModbusCommunication::m_WriteQuery);
    v_AnswerModel=new ModbusDataModel;
}


/*Метод для отправки запроса на запись вызывается из модели запросов методом
 *  v_QueryModel->s_WrieteRegister */
void ModbusCommunication::m_WriteQuery(QModbusDataUnit data, int portId, int Addres)
{
    v_PortModel->m_Query(portId,Addres,data,true);
}

void ModbusCommunication::m_WriteQueryTCP(QByteArray data)
{

    QDataStream l_StreamBuf(data);
    IO_Model_WriteTYpe l_DataBuf;
    l_StreamBuf  >> l_DataBuf;

    Ltype *l_buf= new Ltype[6];

    l_buf[LstQueryEnum::addresClient] = l_DataBuf[G_IO_MODEL::adrContr].toInt();
    l_buf[LstQueryEnum::numb] = 1;
    l_buf[LstQueryEnum::PortIdConnect] = l_DataBuf[G_IO_MODEL::port].toInt();
    l_buf[LstQueryEnum::startS] = l_DataBuf[G_IO_MODEL::adrCell].toInt();
    l_buf[LstQueryEnum::typeReg] = l_DataBuf[G_IO_MODEL::TypeReg].toInt();
    auto l_Query=v_QueryModel->m_formatDataList(l_buf);
    l_Query.setValue(0,l_DataBuf[G_IO_MODEL::rStatus].toInt());
    v_PortModel->m_Query(l_buf[LstQueryEnum::PortIdConnect],l_buf[LstQueryEnum::addresClient],l_Query,true);

}

void ModbusCommunication::m_ConnectDisconnect(bool toggle)
{

}

void ModbusCommunication::m_CreateDeletePort(QString name, bool DelCre)
{

}
/*Открытие порта если порт открылся то добавляем его в  модель v_PortModel
 сохраняем настройки порта в эту же модель  и устанавливаем таймер
и соеденяем ответ от порта с методом m_ResponseSerial*/
void ModbusCommunication::m_OpenPort(QString name, QList<int> settingList)
{
    auto s_bufSerial= new SerialConnect(v_PortModel->rowCount(QModelIndex()));
    // v_serialPortList.append(s_bufSerial);
    if (s_bufSerial->m_OpenPort(name,v_PortModel->m_GetSettingFormIndex(settingList)))
    {

        v_PortModel->m_SaveSetting(settingList);
        v_Timer->setInterval(settingList.at(6));
        v_PortModel->m_AppEndRow(s_bufSerial);


        connect(s_bufSerial,&SerialConnect::s_Ansver,this,&ModbusCommunication::m_ResponseSerial);
    }
    else delete s_bufSerial;

}
/*Запуск и остановка таймера таймер запускается по нажатию кнопки в GUI
Таймер не запустится если созданных портов (v_PortModel) и запросов модбас (v_QueryModel)
не будет */
void ModbusCommunication::m_TimerStopStart(bool StopStart)
{
    if (StopStart && v_PortModel->rowCount(QModelIndex()) && v_QueryModel->rowCount(QModelIndex())) {
        v_Timer->start();
        v_QueryModel->setQueryTimerStatus(true);
    }
    else{
        v_QueryModel->setQueryTimerStatus(false);
        v_Timer->stop();
    }
}
/*Ответ от Последовательного порта IDConnect это адрес MOdbus устройства
результат запроса сначало отправляется в общую модель ответтов (v_AnswerModel)
и добавляет в общий лист , далее ответ (reply) сравниват с моделью отображения ответов на GUI(v_DataControllerModel)
если пользователь выбрас запрос для просмотра и пришел именно ответ этого запроса
от ответ добавляется в модел просмотра */
void ModbusCommunication::m_ResponseSerial(int l_IdConnect, QModbusReply *reply)
{
    QModbusDataUnit bufResult= reply->result();

    v_AnswerModel->m_AddData(reply->serverAddress(),bufResult);
    // qDebug() << l_IdConnect << "Id" << v_DataControllerModel->m_Compare(l_IdConnect,reply->serverAddress(),bufResult.startAddress());
    if (v_DataControllerModel->m_Compare(l_IdConnect,reply->serverAddress(),bufResult.startAddress()))
        for (int i=0;i<bufResult.valueCount();i++){

            v_DataControllerModel->m_AddData((bufResult.startAddress()+i),
                                             bufResult.registerType(),
                                             bufResult.value(i) ,1 );
            //       QList<int>forbuf;
            //       forbuf.append(bufResult.startAddress()+i);
            //       forbuf.append(bufResult.registerType());
            //       forbuf.append(bufResult.value(i));
            //       forbuf.append(true);
            //       buf.append(forbuf);
        }
    //   QHash<int, QList<QList<int>>> hasBuf;
    //   hasBuf[reply->serverAddress()]=buf;
    reply->deleteLater();
    //v_DataModbus[port]=hasBuf;

}
/*вызывается с помощью таймера (this->v_Timer) с определенным интервалом
проверяется если порты (v_PortModel) созданы и модел запросов (v_QueryModel) не пуста.
Итератор (v_iteratorQuery) необходим для пошагового перехода по запросам
запрос отправляется в (v_PortModel->m_Query) с параметрами : порт в который нужно отправить
запрос , адрес модбас устройства ,и сам запрос Полученны с помощью преобразования
методом m_formatDataList из значений итератора (v_iteratorQuery) далее итератор увеличивается
на 1 переходя к следующему запросу после достижения конца итератор переходи в начало на
следующий круг запросов*/
void ModbusCommunication::m_timeOut()
{
    //for ( v_myDataQuery->keys() )
    // v_iterator.key()->at(ModbusDataQueryModel::port)
cycle:
    if (v_PortModel->rowCount(QModelIndex()) && v_QueryModel->rowCount(QModelIndex()))  // v_iterator.value();
        if (v_iteratorQuery!=v_endIteratorQuery)
        {
            //qDebug () << QString::number( **v_iteratorQuery,10) << "TypeS" << v_QueryModel->m_formatDataList( (*v_iteratorQuery)).registerType();
            v_PortModel->m_Query((*v_iteratorQuery)[LstQueryEnum::PortIdConnect],
                    (*v_iteratorQuery)[LstQueryEnum::addresClient],
                    v_QueryModel->m_formatDataList( (*v_iteratorQuery)));
            v_iteratorQuery++;
        }
        else { v_iteratorQuery=v_myDataQuery->begin(); goto cycle;


            // qDebug () <<"timeOut" << v_myDataQuery->size();
        }
}
