import QtQuick 2.9
import QtQuick.Controls 2.2
Item {
    property var l_SerialSettingCreate
//    Rectangle{
//        border.color: "black"
//        anchors.fill: parent
        signal on_SignalServer(int nPort)
        signal on_DeleteController(int index)
//    }
    Rectangle
    {
    anchors.fill: parent
    color: "#34333b"


    Button{
        id: o_ButStartStop
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 20
        anchors.leftMargin: 10
        height: 30
        width: parent.width*0.4
        text: "Server On/Off"

//        background: Rectangle {
//            color: "black"
//            anchors.fill: parent
//            //red"
//        }
        background: Rectangle{color:  GUIRelate.StatusServer ? "#9df084" : "#c5c4cc"  }
        onClicked: {
            on_SignalServer(o_PortNumber.text)
        }


    }
    Text {
        id: o_portString
        anchors{
            left: o_ButStartStop.right
            top: parent.top
            topMargin: 20

        }
        width: 35
        height: o_ButStartStop.height
        text: qsTr("Port:")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "white"

    }
    TextField{
        id:o_PortNumber
        anchors.topMargin: 20
        anchors.rightMargin: 10

        anchors.left: o_portString.right
        anchors.top: parent.top
        anchors.right: parent.right
        height: o_ButStartStop.height
        text: "2323"


    }
    Button{
        id:o_ControllerAdd
        anchors.top: o_ButStartStop.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        width: 100
        height: 30
        text:  "controllerAdd"
        property var obj_Create
        onClicked: {
           // o_ControllerSettingForm.visible=!o_ControllerSettingForm.visible
            if (!obj_Create)
            {
            var comp;
            var obj;
            comp = Qt.createComponent("PortSettingForm.qml")
            obj_Create=comp.createObject(o_Root, {"id" : "o_PortSettingForm" ,
                                  "z" :2 ,
                                  "width" : 450});
            obj_Create.anchors.centerIn=o_Root
            } else obj_Create.destroy();
//            CotrollerSettingForm{
//                id: o_ControllerSettingForm
//                anchors.centerIn: parent
//               // height: 300
//                z:2
//                width: 450
//                visible: false
//            }
        }
    }

    Item {
        id: o_ControllerItemView
        anchors.top: o_ControllerAdd.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 10
        anchors.leftMargin: 10
        anchors.rightMargin: 20
        height: 500

    ListView {
        anchors.fill: parent
        spacing: 2
        id: o_ControllerView

        //width: parent.width
        //height: (parent.height-o_ButStartStop.height-topMargin)/2
        model: o_PortListModel

        delegate: Item {
           // border.color: "black"

            height: 25
            width: parent.width
            Button {
                id: o_ControllerName
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: o_ControllerDelete.left
                text: names
                background: Rectangle {color: "#656c8c"}


                onClicked:
                {
                    if (l_SerialSettingCreate) l_SerialSettingCreate.destroy()
                    else{
                    var comp=Qt.createComponent("SerialSetting.qml")

                   l_SerialSettingCreate=comp.createObject(o_Root, {"id" : "o_SerialSetting" ,
                                              "z" :3, "color" : "#cacaca"});

                    l_SerialSettingCreate.anchors.centerIn= o_Root
                      l_SerialSettingCreate.v_Select=index;
                      l_SerialSettingCreate.v_setting=o_PortListModel.m_GetSettingToView(index)
                     // o_ControllerView.l_ObjCreate.color = "#cacaca"
                    }
                    // o_AddQueryForm.visible=!o_AddQueryForm.visible
                    //            id: o_AddQueryForm
                    //            anchors.horizontalCenter: parent.horizontalCenter
                    //            anchors.top: parent.top
                    //            anchors.topMargin: 100
                    //            z:3
                    //            width: 450

                    //            visible: false
                }
            }
            Button {
                background: Item {
                    id: name
                }
                opacity: 100
                id : o_ControllerDelete
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: height
               // color: status ? "green" : "red"
                icon.source: status ? "/icon/connect" : "/icon/disconnect"
                padding: 0
                icon.color:  status ? "transparent" : "#f1fa73"

//                MouseArea
//                {
//                    anchors.fill: parent
                    onClicked: {
                       // on_DeleteController(index)
                        o_PortListModel.m_ConnectDevice(index)
                    }
                    onPressAndHold: {
                        if (l_SerialSettingCreate) l_SerialSettingCreate.destroy()
                        o_PortListModel.m_DeleteConndected(index);

                    }
                }


        }
    }
    }
    ListView {
        id: o_SetFilterView
        anchors.top: o_ControllerItemView.bottom
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.topMargin: 30
        anchors.bottomMargin: 20
        anchors.leftMargin: 20
        width: 100
        model: o_DataControllerModel
        delegate: Rectangle{
            height: 30
            border.color: "black"
            width: parent.width
            Text {
                id: name
                text: regAdr
            }
        }
    }
    }
}
