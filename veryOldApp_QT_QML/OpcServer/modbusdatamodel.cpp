#include "modbusdatamodel.h"
#include <enumproj.h>
#include <QDebug>
#define SizeArray 5
ModbusDataModel *ModbusDataModel::m_data;

ModbusDataModel::ModbusDataModel()
{

//m_data=this;
    int col=1;
    int row=50;
    //         for (int i=0;i<row;i++)
    //         {
    //             QHash<int,QVariant> buf;
    //             buf[id]=i;
    //                     buf[Alias]="fsdfs";
    //                     buf[status]= (i%2==0) ? 1 :0 ;
    //             {
    //                 v_AbstractModelQML.append(buf);
    //             }
    //         }


    QHash<int,QVariant> buf;
    for (int i=0;i<30;i++)
        buf[20+i]="mVal" + QString::number(i,10);


    //v_DataModbus[10]=buf;
    //     buf[id]=1;
    //     buf[Alias]="pump";
    //     buf[status]= 1;
    //     buf[Type]= Et::A_DO;


    buf[21]="mVal 21";
    buf[id]=2;
    buf[Alias]="motor";
    buf[status]= 0;
    buf[Type]= Et::A_DO;
    //  v_AbstractModelQML.append(buf);

    buf[id]=3;
    buf[Alias]="Staus motor";
    buf[status]= 0;
    buf[Type]= Et::A_DI;
    // v_AbstractModelQML.append(buf);

    buf[id]=4;
    buf[Alias]="Status pump";
    buf[status]= 1;
    buf[Type]= Et::A_DI;
    // v_AbstractModelQML.append(buf);

    buf[id]=5;
    buf[Alias]="Energy Currency";
    buf[status]= 10;
    buf[Type]= Et::A_AI;
    //  v_AbstractModelQML.append(buf);

    buf[id]=6;
    buf[Alias]="Energy Voltage";
    buf[status]= 220;
    buf[Type]= Et::A_AI;
    //  v_AbstractModelQML.append(buf);

    buf[id]=7;
    buf[Alias]="Brightness Lighting";
    buf[status]= "10%";
    buf[Type]= Et::A_AO;
    //  v_AbstractModelQML.append(buf);

    buf[id]=8;
    buf[Alias]="Power of Heating";
    buf[status]= "20%";
    buf[Type]= Et::A_AO;
    //  v_AbstractModelQML.append(buf);

    // m_formatModels();
}

QHash<int, QByteArray> ModbusDataModel::roleNames() const
{
    QHash<int,QByteArray> buf;
    QMetaEnum metaBuf=QMetaEnum::fromType<my_roleD>();
    // int x=0;
    for (int i=0,x=0; (x=metaBuf.value(i))>0;i++)
    {
        buf[x]=metaBuf.key(i);
    }
    return buf;
}

QVariant ModbusDataModel::data(const QModelIndex &index, int role) const
{
    int x=0;
    for (const auto &find : v_DataModbus){
        int l=x;
        x+=find.size();
        //qDebug () << x << "Xdata" << index.row();
        if (x<(index.row()+1)) continue;
        else
        {
            if (role==status) return find.at(index.row()-l)[1];
            if (role==IdController) return v_DataModbus.key(find).at(0);
            if (role==adrCell) return find.at(index.row()-l)[0];
        }

    }
    return QVariant("Null");
}

int ModbusDataModel::rowCount(const QModelIndex &parent) const
{
    int x=0;
    for (const auto &find : v_DataModbus)
        x+=find.size();

    return x;
}
/*callback*/
QHash<QList<int>, QList<QList<int> > > ModbusDataModel::m_getAllFunc()
{
    qDebug () << "return" << m_data->m_formatModels().keys();
    return m_data->m_formatModels();
}

QHash<QList<int>, QList<QList<int> > > ModbusDataModel::m_formatModels()
{
    //    adrCell,status,Alias,Type
    /*type register  */
    QHash<QList<int>, QList<QList<int>>> bufer;
/*format sender  key (idAdressCOntroller, PortID) , Value (AdressCel,Status,Alisas,Type)   */
    for (auto &l_find : v_DataModbus.keys()){
        bufer[l_find];
        for (auto j_find : v_DataModbus.value(l_find))
        {
            QList<int> j_Buf;
            j_Buf.clear();
            for (int i=0;i <5 ;i++)
                j_Buf.append(j_find[i]);
            bufer[l_find].append(j_Buf);
        }
    }
    qDebug () << v_DataModbus.keys().at(0) <<"keysModel";
    return bufer;
    //    QHash<int ,QList<QHash<int, QVariant> > > bufSend;
    //     QList<QHash<int, QVariant> > buf;
    //       if (v_DataModbus.size())
    //        {
    //            for (int i=0;i<v_AbstractModelQML.size();i++){
    //                int x=v_AbstractModelQML.at(i).value(Type).toInt();
    //                switch (x) {
    //                case Et::A_AI:
    //                    bufSend[Et::A_AI].append(v_AbstractModelQML.at(i));
    //                    break;
    //                case Et::A_AO:
    //                    bufSend[Et::A_AO].append(v_AbstractModelQML.at(i));
    //                    break;
    //                case Et::A_DI:
    //                    bufSend[Et::A_DI].append(v_AbstractModelQML.at(i));
    //                    break;
    //                case Et::A_DO:
    //                    bufSend[Et::A_DO].append(v_AbstractModelQML.at(i));
    //                    break;
    //                default:
    //                    break;
    //                }
    //            }
    //       //qDebug() <<     <<"TypesAllmod";
    //       }
    //    qDebug () << bufSend;

   // emit sendAll(Et::m_TforByte(Et::AllIOModel,buf));
}

// int PortId ,int AdrCont, array[reg, type, data, statusUpdata]



QList<int> ModbusDataModel::ModDataList ()
{

}

/*перебор массива еси Id контроллера (findi==Id) добавлен то начинаем искать регистры
если регистр в диапазоное (startReg >=findj <=endReg) если регистры найдены то
перезаписываем найденые регистры добавляем в list v_Finded после чего проверяем если
количество найденных регистров не совпадает с колличествомрегистров в answer (countReg)
перебераем ответ ansver заново исключая найденные регистры и добавляем новые регистры в модель*/
void ModbusDataModel::m_AddData(int IdCont, QModbusDataUnit data)
{
    //    for (int i=0;i<data.valueCount();i++)
    //    {
    //    qDebug () <<data.value(i) << "val" << data.startAddress()+i << "adr" << IdCont <<"adrId";
    //    }
    // return;
    QList<QList<int>> buf;
    //    IdController,
    QList<int> v_Finded;
    v_Finded.clear();
    //    adrCell,status,Alias,Type
    /*type register  */
    auto ansver = data;
    int v_TypeReg = ansver.registerType();
    const int countReg=ansver.valueCount();
    const int startReg = ansver.startAddress();
    const int endReg =startReg+ansver.valueCount()-1;

    for (auto &findi: v_DataModbus.keys())
        if (findi.at(0)==IdCont)
        {
            for ( auto findj : v_DataModbus.value(findi))
                //if (findj.at(0)ansver.)
            {
                //qDebug () << findj[1] << "findj";
                if (*findj>=startReg && endReg >= *findj)
                {
                    findj[1]=ansver.value(*findj-startReg);
                    findj[3]= v_TypeReg;
                    v_Finded.append(*findj);
                }

                //                for (register int i=0; i<ansver.valueCount(); i++)
                //            find.replace(2,data);
                //            find.replace(3,statusUpdates);
                //            endResetModel();
            }
            if (v_Finded.size()!=countReg)
                for (int i=0; i<countReg;i++)
                {
                    if ( v_Finded.indexOf(startReg+i)>-1) continue;

                    int *b=new int[5];
                    *b=startReg+i;
                    b[1]=ansver.value(i);
                    b[3]=v_TypeReg;
                    // qDebug () <<*b <<"Adress" << ansver.value(i) << "val" << v_TypeReg << "TypeReg";
                    //v_DataModbus.find
                    qDebug () << findi.at(0) << "FindI";
                    v_DataModbus[findi].append(b);
                }
            //            for (int m=0; m< this->rowCount(QModelIndex());m++)
            //            {

            //            //qDebug () << this->data(this->index(m),status) << "status " + QString::number(m);
            //            }
            //            for (auto &fdf : v_DataModbus)
            //            qDebug () << fdf.size() << "Size";
            endResetModel();
            return;
        }
}

void ModbusDataModel::m_AddId(int Id , int PortName)
{
    //  if (v_DataModbus.find(Id)!=v_DataModbus.end()) return;

    // auto *db=new int[5]{0,0,0,0,0};
    QList<int> buf;
    buf.append(Id);
    buf.append(PortName);
    qDebug () << Id <<"AddresId";
    v_DataModbus[buf];//.append(db);

}
