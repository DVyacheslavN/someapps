#ifndef SEVETN_H
#define SEVETN_H
#include <sabstractevent.h>

class SEvetn : public SabstractEvent
{
    Q_OBJECT
public:
    SEvetn();
    void sl_addEvent(QString str, Et::EventT type);
};

#endif // SEVETN_H
