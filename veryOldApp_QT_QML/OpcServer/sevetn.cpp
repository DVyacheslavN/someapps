#include "sevetn.h"
#include <QDebug>
#include <QDateTime>

SEvetn::SEvetn()
{

}

void SEvetn::sl_addEvent(QString str, Et::EventT type)
{
    if (str==nullptr)  {qDebug()<< "Event-> addEvent ->str is null" ; return;}
    if (type==Et::Alarm)
    {
        QList<QString> buf;
        buf <<str;
        QDateTime data;
        buf << data.currentDateTime().toString("HH:mm -> dd.MM");
        emit si_Alarm(buf);
    }

    emit si_ShowEvent(str ,type);
}
