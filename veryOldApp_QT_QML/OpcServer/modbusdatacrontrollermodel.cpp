#include "modbusdatacrontrollermodel.h"
#include <QDebug>
ModbusDataCrontrollerModel::ModbusDataCrontrollerModel()
{

   // v_ModelData.append({11,11,11,11} );
}

QHash<int, QByteArray> ModbusDataCrontrollerModel::roleNames() const
{
//    QMetaEnum v_metaBuf= QMetaEnum::fromType<my_role>();
//    QHash<int,QByteArray> v_BufHash;
//    int x=0;
//    for (int i=0; (x=v_metaBuf.value(i))>0;i++)
//    {
//        v_BufHash[x]=v_metaBuf.key(i);
//    }
//    return v_BufHash;

    auto v_metaBuf= QMetaEnum::fromType<my_roless>();
    QHash<int,QByteArray> v_BufHash;
    for (int i=0,x=0 ; (x=v_metaBuf.value(i))>0 ;i++) {
        v_BufHash[x]=v_metaBuf.key(i);
    }

    qDebug () << v_BufHash << "buf";
    return v_BufHash;
}
 //array[regAdr, type, data, statusUpdata]
QVariant ModbusDataCrontrollerModel::data(const QModelIndex &index, int role) const
{
   const auto buf= v_ModelData.at(index.row()) ;
   if (role==regAdr) return QVariant( buf.at(0));
   if (role==type) return QVariant( buf.at(1));
   if (role==mydata) return QVariant( buf.at(2));
   if (role==statusUpdate) return QVariant(buf.at(3));
   return QVariant();

}

int ModbusDataCrontrollerModel::rowCount(const QModelIndex &) const
{
    return v_ModelData.size();
}
/*Добавление данные в модель если адрес регистра regAddress и тип регистра regType есть в модели
то перезаписываем данные по существующему регистру , если нет то добавляем новые данные */
void ModbusDataCrontrollerModel::m_AddData(int regAdres, int regType, int data, int statusUpdates)
{
 //   qDebug () << data << "new Data";
    int i=0;
    for (auto &find: v_ModelData )
    {
        if (find.at(0)==regAdres && find.at(1)==regType)
        {
            find.replace(2,data);
            find.replace(3,statusUpdates);
            dataChanged(index(i),index(i));
            return;
        }
        i++;
    }
    QList<int> s_ListBuf ;
    s_ListBuf <<regAdres <<regType << data << statusUpdates;
    v_ModelData.append(s_ListBuf);
    endResetModel();
}

