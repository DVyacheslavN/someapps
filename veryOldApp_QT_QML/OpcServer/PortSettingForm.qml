import QtQuick 2.0
import QtQuick.Controls 2.2
Item {
    property string textToType
    signal s_AddNewController(int type, string prop)
    property var v_ItemHeight :210

    height: v_ItemHeight
    property var previousY
    property var previousX
            MouseArea{
                anchors.fill: parent
                onPressed: {
                    // Запоминаем позицию по оси Y
                    previousY = mouseY
                    previousX = mouseX
//                    parent.setY(parent.y + 100)
//                    parent.setHeight(parent.height - 100)
                }
                onMouseYChanged: {
                    //console.log(mouseY)
                    var dy = mouseY - previousY
                    var dx = mouseX - previousX
                    parent.y= (parent.y + dy)
                    parent.x =(parent.x + dx)
                   // parent.setHeight(parent.height - dy)
                     parent.anchors.centerIn=undefined
                }
            }
    Rectangle{
        color: "#cacaca"
        anchors.fill: parent

        ComboBox{
            id: o_ComboSelectType
            anchors.top: parent.top
            anchors.margins: 20
            anchors.right: parent.right
            height: 30
            width: 100
            model:  ["TCP", "COM"]
            currentIndex: 1
            onActivated: {
                if (index==0)
                    textToType="TCP"
                else textToType="COM"
            }
            Component.onCompleted: {
                if (o_ComboSelectType.currentIndex==0)
                    textToType="TCP"
                else textToType="COM"
            }
        }
        Button {
            id: o_SerialSettingButton
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 20
            height: 30
            width: 150
            text: "Setting Serial"
            visible: o_ComboSelectType.currentIndex
            onClicked: {
                o_SerialSetting.visible=true
            }
        }

        Item {
            id: o_rect
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: o_ComboSelectType.bottom
            anchors.bottom: parent.bottom
            anchors.margins: 20
            Item {
                id:o_IpAndComEdit
                anchors.left: parent.left
                anchors.top: parent.top
                width: parent.width/2
                height: 30
                TextField{
                    id: o_IpAddresTEXT
                    anchors.fill: parent
                    text:"Ip addres"
                    visible: {o_ComboSelectType.currentIndex ? 0 : 1}
                }
                ComboBox
                {
                    id: o_SerialSelect
                    anchors.fill: parent
                    model: o_PortListModel.PortNames // mod_serialList
                    visible: {o_ComboSelectType.currentIndex ? 1 : 0}
                }
            }
            Text {
                id: o_IpAndComComment
                anchors.left: o_IpAndComEdit.right
                anchors.top: parent.top
                anchors.right: parent.right
                height: 30
                verticalAlignment: Text.AlignVCenter
                text: qsTr("Set " + textToType)
            }
            ComboBox {
                id:o_TimeSelectCombo
                anchors.left: parent.left
                anchors.top: o_IpAndComEdit.bottom
                anchors.topMargin: 15
                width: parent.width/2
                height: 30
                model:  [50,100,200,500,800,1000]
            }
            Text {
                id: o_TimeSelectComboComment
                anchors.left: o_TimeSelectCombo.right
                anchors.top: o_IpAndComComment.bottom
                anchors.topMargin: 15
                anchors.right: parent.right
                verticalAlignment:  Text.AlignVCenter
                height: 30
                text: qsTr("Time Update")
            }
            Button {
                id: o_Cancel
                anchors.top: o_TimeSelectCombo.bottom
                anchors.left: parent.left
                width: parent.width/2
                height: 30
                anchors.topMargin:  20
                text: "cancel"
                onClicked:  parent.parent.parent.destroy() // o_ControllerSettingForm.destroy()
            }
            Button{
                anchors.top: o_TimeSelectComboComment.bottom
                anchors.right: parent.right
                anchors.left: o_Cancel.right
                height:  30
                anchors.topMargin:   20
                anchors.leftMargin:  20
                text: "Add "+ textToType
                onClicked:
                {
                    GUIRelate.m_connnecting(o_SerialSelect.currentText,o_SerialSetting.v_arraySetting)
//                    if(o_ComboSelectType.currentIndex)
//                    s_AddNewController(o_ComboSelectType.currentIndex,o_IpAddresTEXT.text)
//                    else s_AddNewController(o_ComboSelectType.currentIndex,o_SerialSelect.currentText)
                }
            }
        }
        SerialSetting {
            id: o_SerialSetting
            anchors.centerIn: parent
            z:3
            visible: false
            color: parent.color
        }
    }

}
