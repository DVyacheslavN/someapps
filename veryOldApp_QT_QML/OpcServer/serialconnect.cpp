#include "serialconnect.h"
#include "QSerialPortInfo"
#include <QDebug>
#include <QModbusClient>

SerialConnect::SerialConnect(int l_idConnect, QObject *parent) : QObject(parent), v_idConnect(l_idConnect)
{


    //QSerialPort *o_Port=new QSerialPort;
    //connect(o_Port,SIGNAL(channelReadyRead(int)),this,SLOT(m_ReadyRead()));
    // modbusDevice->connectionParameter
    modbusDevice=new QModbusRtuSerialMaster();
}

QStringList SerialConnect::m_findSerialPort()
{
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        v_serialPortList.append(info.portName());

    return v_serialPortList;
}

bool SerialConnect::m_OpenPort(QString name, QList<int> setting)
{
    connect(modbusDevice,&QModbusRtuSerialMaster::stateChanged,this, &SerialConnect::m_stateCnahged);
    connect(modbusDevice, &QModbusClient::errorOccurred, [this](QModbusDevice::Error) {
        qDebug () << modbusDevice->errorString() << "ErrMOd";
        modbusDevice->disconnectDevice();

        emit s_EventSerial( modbusDevice->errorString(),Et::Alarm);
    });
    modbusDevice->setConnectionParameter(QModbusDevice::SerialPortNameParameter,name);
    m_SetSettingPort(setting);
    if(!modbusDevice->connectDevice())
    {
        qDebug () << "Connected failed";
        emit s_EventSerial( "Connected failed",Et::Alarm);
        return false;
    }

    else
        emit s_PortAdded(name,(modbusDevice->state()==modbusDevice->ConnectedState) ? true : false);
    return true;

}

void SerialConnect::m_SetSettingPort(QList<int> setting)
{
    //Q_ASSERT;
    bool connecBit=false;
    if (modbusDevice->state()!=modbusDevice->UnconnectedState){ modbusDevice->disconnectDevice(); connecBit=true;}
    if (modbusDevice)
    {

        modbusDevice->setConnectionParameter(QModbusDevice::SerialParityParameter,//QSerialPort::NoParity);
                                             setting.at(0));
        modbusDevice->setConnectionParameter(QModbusDevice::SerialBaudRateParameter,//QSerialPort::Baud19200);
                                             setting.at(1));
        modbusDevice->setConnectionParameter(QModbusDevice::SerialDataBitsParameter,//QSerialPort::Data8);
                                             setting.at(2));
        modbusDevice->setConnectionParameter(QModbusDevice::SerialStopBitsParameter,//QSerialPort::OneStop);
                                             setting.at(3));

    modbusDevice->setTimeout(setting.at(4));
    modbusDevice->setNumberOfRetries(setting.at(5));
    qDebug() << setting;
    }
    if (connecBit) modbusDevice->connectDevice();
}

void SerialConnect::m_Query(int ControllerAdres, QModbusDataUnit s_Query ,bool Write)
{
    QModbusReply *reply=nullptr;
   // qDebug () <<s_Query.value(0) << "value";
    if (Write) reply=modbusDevice->sendWriteRequest(s_Query,ControllerAdres);
    else reply=modbusDevice->sendReadRequest(s_Query,ControllerAdres); //else read
    if (reply)
    {  // addres 10
        if (!reply->isFinished())
            connect(reply, &QModbusReply::finished, this, &SerialConnect::m_ReadyRead);
        else
            delete reply; // broadcast replies return immediately
    } else {
        s_EventSerial("ReadError" + modbusDevice->errorString(),Et::Alarm);
    }
}

void SerialConnect::m_stateCnahged(QModbusDevice::State state)
{
    qDebug () <<  modbusDevice->state() << "State";
    QString str= modbusDevice->connectionParameter(QModbusDevice::SerialPortNameParameter).toString();
    emit s_EventSerial ( (modbusDevice->state()==modbusDevice->ConnectedState) ? "connected " +str : "connected failed " +str ,Et::Event);
    emit s_StausChanged();

}

void SerialConnect::ts_TimeSendQuery()
{
    QModbusDataUnit data (QModbusDataUnit::HoldingRegisters,5,2);
    qDebug () << "Query";
    // m_Query(&data);

}

void SerialConnect::m_ReadyRead()
{
    //QString Adr, QHash<int, QVariant>  data
    auto reply = qobject_cast<QModbusReply *>(sender());
    if (!reply) return;
    auto buf= new QHash <int ,QVariant> ;
    int AdrController =  reply->serverAddress();
    if (reply->error() == QModbusDevice::NoError)
    {
        const QModbusDataUnit unit = reply->result();
        for (uint i = 0; i < unit.valueCount(); i++) {
            (*buf)[unit.startAddress()+i]=unit.value(i);

            //            const QString entry = tr("Address: %1, Value: %2").arg(unit.startAddress() + i)
            //                                     .arg(QString::number(unit.value(i),
            //                                          unit.registerType() <= QModbusDataUnit::Coils ? 10 : 16));
            //            ui->readValue->addItem(entry);
         //   qDebug () << "Serial m_readyread" << unit.value(i);
        }

        //  emit s_Ansver(modbusDevice->connectionParameter(QModbusDevice::SerialPortNameParameter).toString(),AdrController,buf);
      emit s_Ansver(v_idConnect,reply);
    }
    else if (reply->error() == QModbusDevice::ProtocolError) {
        s_EventSerial(QString("Read response error: %1 (Mobus exception: 0x%2)").
                      arg(reply->errorString()).
                      arg(reply->rawResult().exceptionCode(), -1, 16), Et::Alarm);
    } else {
        s_EventSerial( QString("Read response error: %1 (code: 0x%2)").
                       arg(reply->errorString()).
                       arg(reply->error(), -1, 16), Et::Alarm);
    }

    //reply->deleteLater();
}
