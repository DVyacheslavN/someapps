#ifndef SABSTRACTMODELQML_H
#define SABSTRACTMODELQML_H
#include <QObject>
#include <QMetaEnum>
#include <QAbstractTableModel>
class SAbstractModelQml : public QAbstractTableModel
{
    Q_OBJECT
public:
    SAbstractModelQml();

    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QHash<int,QByteArray> roleNames() const;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent=QModelIndex()) const;
    virtual void m_roleSetFromEnum();
    Q_INVOKABLE virtual void removeRow(int row, const QModelIndex &parent=QModelIndex());
    Q_INVOKABLE virtual QVariant m_data (int row, int role);
     //enum my_roles {};
    QMetaEnum  v_rolseEnumStr;
    QList <QHash<int, QVariant>> v_AbstractModelQML;
    enum ALLrole {
        id=1000,
        Alarm,
        Time,
        status,
        ip,
        port,
        Alias,
        name,
        Type
    };
    Q_ENUM(ALLrole)
signals:
    void v_siseRowChanged();
private:



public slots:
    virtual int v_siseRow();
    virtual void sl_WriteAll(QList<QHash<int, QVariant> > *data);
};

#endif // SABSTRACTMODELQML_H
