#ifndef SITEMSMODEL_H
#define SITEMSMODEL_H
#include <sabstractmodelqml.h>

class SItemsModel : public SAbstractModelQml
{
    Q_OBJECT
public:
    SItemsModel();
    enum my_role {
        id=Qt::UserRole+1,
        controller,
        type,
        value
    };
    Q_ENUM(my_role)
    void m_roleSetFromEnum();
};

#endif // SITEMSMODEL_H
