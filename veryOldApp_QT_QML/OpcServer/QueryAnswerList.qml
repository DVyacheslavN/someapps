import QtQuick 2.0
import QtQuick.Controls 2.2
Item {
  //  anchors.fill: parent
   // anchors.topMargin: 20
   // [type,typeCon,conAdr,stAdr,numb]
    property var l_setting: [0,0,0,0,0]
    ListView{
        anchors.fill: parent
        model:o_DataControllerModel
        delegate: o_deleganeDataFromQuery
    }
    Component
    {
        id: o_deleganeDataFromQuery
        Item{
            anchors.left: parent.left
            anchors.right: parent.right
            height: 20
            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left:parent.left
                width: 100
                id: l_registerName
                text: "register: " + regAdr
            }
            Text {
                id: l_dataName
                anchors{
                    left: l_registerName.right
                    top: parent.top
                    bottom: parent.bottom
                }
                width: 100
                text: "data: " + mydata
            }
            Button
            {
                anchors{
                    left: l_dataName.right
                    top: parent.top
                    bottom: parent.bottom
                }
                width: height
                onPressed:
                {
                    if (!o_WriteFormCreate.l_ObjecCreate)o_WriteFormCreate.onClicked();
                    if (o_WriteFormCreate.l_ObjecCreate)
                    o_WriteFormCreate.l_ObjecCreate.settings=[l_setting[0],l_setting[1],l_setting[2],regAdr,mydata]
                }
            }
        }
    }
}
