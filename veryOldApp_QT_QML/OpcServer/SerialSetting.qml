import QtQuick 2.8
import QtQuick.Controls 2.2
Rectangle {

    height:15*2+15*3+30*3+45
    width: 15*2+50+150*2
    property int v_Select: -1
    property var previousY
    property var previousX
    property var v_setting: [0,4,3,0,1000,3]
    property var v_arraySetting: [o_Parity.currentIndex, o_Baud.currentIndex, o_Data.currentIndex,
        o_Stop.currentIndex,o_responseTime.text,o_numberOfRetries.text,100]// o_TimeSelectCombo.currentText
    MouseArea{
        anchors.fill: parent
        onPressed: {
            // Запоминаем позицию по оси Y
            previousY = mouseY
            previousX = mouseX
            //                    parent.setY(parent.y + 100)
            //                    parent.setHeight(parent.height - 100)
        }
        onMouseYChanged: {
            //console.log(mouseY)
            var dy = mouseY - previousY
            var dx = mouseX - previousX
            parent.y= (parent.y + dy)
            parent.x =(parent.x + dx)
            // parent.setHeight(parent.height - dy)
            parent.anchors.centerIn=undefined
        }
    }


    Column {
        anchors.fill: parent
        anchors.margins: 15
        spacing: 15

        Row
        {
            spacing: 50
            anchors.left: parent.left
            anchors.right: parent.right
            // anchors.margins: 15
            height: 30

            ComboBox{
                id: o_Parity
                width: 150
                // text:   GUIRelate.SettingList[0]
                height: parent.height
                model: o_PortListModel.prop_Parity()
                currentIndex: v_setting[0]

                //            onEditingFinished:
                //            {
                //                GUIRelate.setSettingList(0,text)
                //            }
            }
            ComboBox{
                id: o_Baud
                width: 150
                model: o_PortListModel.prop_Baud()
                height: parent.height
                currentIndex: v_setting[1]
                //            onEditingFinished:
                //            {
                //                GUIRelate.setSettingList(1,text)
                //            }
            }

        }
        Row
        {
            spacing: 50
            anchors.left: parent.left
            anchors.right: parent.right
            // anchors.margins: 15
            height: 30

            ComboBox{
                id: o_Data
                width: 150
                // text: GUIRelate.SettingList[2]
                height: parent.height
                model:  o_PortListModel.prop_Data()
                currentIndex: v_setting[2]
                //            onEditingFinished:
                //            {
                //                GUIRelate.setSettingList(2,text)
                //            }
            }
            ComboBox{
                id: o_Stop
                width: 150
                // text: GUIRelate.SettingList[3]
                height: parent.height
                model: o_PortListModel.prop_Stop()
                currentIndex: v_setting[3]
                //            onEditingFinished:
                //            {
                //                GUIRelate.setSettingList(3,text)
                //            }
            }

        }

        Row
        {
            spacing: 50
            anchors.left: parent.left
            anchors.right: parent.right
            // anchors.margins: 15
            height: 45

            Column{
                height: parent.height
                width: 150
                Text {

                    text: qsTr("responseTime")
                    height: 15
                }
                TextField{
                    id: o_responseTime
                    width: 150
                    text: v_setting[4]
                    height: 30
                }
            }
            Column{
                height: parent.height
                width: 150
                Text {

                    text: qsTr("numberOfReturns")
                    height: 15
                }
                TextField{
                    id: o_numberOfRetries
                    width: 150
                    text: v_setting[5]
                    height: 30
                }
            }

        }

        Row
        {
            spacing: 50
            anchors.horizontalCenter: parent.horizontalCenter
            // anchors.margins: 15
            height: 30

            Button{
                height: parent.height
                text: "set"
                onClicked:{
                    if (v_Select<0) o_SerialSetting.visible=false
                    else{
                        o_PortListModel.m_SetSettingToModel(v_Select,v_arraySetting)
                        parent.parent.parent.destroy();
                    }
                }
                // onClicked: o_SerialSetting.v_arraySetting.push()
            }
            Button{
                height: parent.height
                text: "cancel"
                onClicked: {
                    if (v_Select<0) o_SerialSetting.visible=false
                    else parent.parent.parent.destroy();
                }
            }
        }
    }
}
