#ifndef ABSTRACTGUIQML_H
#define ABSTRACTGUIQML_H
#include <QObject>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <enumproj.h>
class AbstractGuiQML : public QObject
{
    Q_OBJECT
public:
    AbstractGuiQML();
    QQmlContext *o_Context;
    QQmlApplicationEngine *o_Engine;
    QObject *v_RootObject;
    Q_INVOKABLE virtual void sl_WindowQmlCreate()=0;
    /*получение информации об ошибкак хи оттображение ее на GUI*/
    Q_SLOT virtual void sl_GetAlarm(QString ,Et::EventT)=0;
Q_SIGNALS:
        /*сигдал */
      void si_ConnectTo(QString IP,int nPort);
      void si_SetAlias(QString *alias);
    //  void si_SetNewStatusItem(int newStatus);
      void si_ShowMessageAlarm(QString str);
      void si_BarMessage(QString str);
     // void si_AddItemConnectList(QString str);
};

#endif // ABSTRACTGUIQML_H
