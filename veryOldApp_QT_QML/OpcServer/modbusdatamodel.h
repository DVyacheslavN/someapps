#ifndef MODBUSDATAMODEL_H
#define MODBUSDATAMODEL_H
#include <QAbstractListModel>
#include <QModbusReply>
#include <QMetaEnum>
//#include <modbusdatacrontrollermodel.h>
class ModbusDataModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QList<int> ModDataList READ ModDataList NOTIFY ModDataListChanged)
public:
    ModbusDataModel();
// int PortId ,Model Conrtoller,
    QHash<QList<int>, QList<int*> > v_DataModbus;
    QHash<int,QByteArray> roleNames()const override;
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;
    static ModbusDataModel *m_data;
    static QHash<QList<int>, QList<QList<int>>> m_getAllFunc ();
    enum my_roleD{
        id=Qt::UserRole+1,
        IdController,
        adrCell,status,Alias,Type

    };
    Q_ENUM(my_roleD)
//    enum ALLrole {
//        id=1000,
//        Alarm,
//        Time,
//        status,
//        ip,
//        port,
//        Alias,
//        name,
//        Type
//    };
//    Q_ENUM(ALLrole)
//    void m_roleSetFromEnum();
public slots:
    QHash<QList<int>, QList<QList<int> > > m_formatModels();
    QList<int>  ModDataList ();
    void m_AddData(int IdCont, QModbusDataUnit data);
    void m_AddId(int Id, int PortName);
signals:
    void ModDataListChanged();
    void sendAll(QByteArray data);
};

#endif // TS_DOMODEL_H
