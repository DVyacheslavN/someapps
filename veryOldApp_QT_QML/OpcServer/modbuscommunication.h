#ifndef MODBUSCOMMUNICATION_H
#define MODBUSCOMMUNICATION_H

#include <QObject>
#include <QModbusDataUnit>
#include <QTimer>
#include <serialconnect.h>
#include <modbusdataquerymodel.h>
#include <portlistmodel.h>
#include <modbusdatamodel.h>
#include <QHash>
class ModbusCommunication : public QObject
{
    Q_OBJECT
public:

    ModbusCommunication();

public slots:
    //void m_SetTimeItemUpdate(int time_M_Sec=500) {v_Timer->setInterval(time_M_Sec);}
    /* получение копии листа запросов модбас создание итераторов
    и добавление в общую модель ответных данные (v_AnswerModel) адреса контроллера */
    void m_GetModbusDataUnit(QList<Ltype*> *data){v_myDataQuery=data;
                                                  v_iteratorQuery=v_myDataQuery->begin();
                                                  v_endIteratorQuery=v_myDataQuery->end();
                                                  for (auto l_find: *v_myDataQuery)
                                                   v_AnswerModel->m_AddId(l_find[LstQueryEnum::addresClient],l_find[LstQueryEnum::PortIdConnect]);}
    void m_WriteQuery (QModbusDataUnit data, int portId, int Addres);
    void m_WriteQueryTCP (QByteArray data);
    void m_ConnectDisconnect(bool toggle);
    void m_CreateDeletePort(QString name, bool DelCre);/*m_OpenPort  метод вызывается сигналом SGuiQml::s_OperPort и открывает порт согласна установленным настройкам (settingList)*/
    void m_OpenPort(QString name, QList <int> settingList);
    void m_TimerStopStart (bool StopStart); /*вызывается сигналом SGuiQml::s_StartStopQueryTimer*/
    void m_ResponseSerial (int l_IdConnect, QModbusReply *reply); /*вызывается SerialConnect::s_Ansver ответ на запрос*/
signals:
    void s_StatsServer(int Status);
    void s_StartStopCnange(bool);
    void s_EventShow(QString , Et::EventT);
private:
    QTimer *v_Timer;
    QList <Ltype*> *v_myDataQuery;
    QList <Ltype*> :: iterator v_iteratorQuery;
    QList <Ltype*> :: iterator v_endIteratorQuery;


private slots:
    void m_timeOut();
public:
    //ModbusDataModel *v_DataModel;
    ModbusDataQueryModel *v_QueryModel;
    PortListModel *v_PortModel;
    QList <SerialConnect*> v_serialPortList ;
    ModbusDataCrontrollerModel *v_DataControllerModel;
    ModbusDataModel *v_AnswerModel;

};
#endif // MODBUSCOMMUNICATION_H
