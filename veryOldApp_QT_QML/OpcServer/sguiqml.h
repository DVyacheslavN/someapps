#ifndef SGUIQML_H
#define SGUIQML_H
#include <abstractguiqml.h>

class SGuiQml :public AbstractGuiQML
{
    Q_OBJECT
    Q_PROPERTY(bool StatusServer READ StatusServer WRITE setStatusServer NOTIFY StatusServerChanged)
    bool v_SerRedy=false;
    QList<int> v_Settings;
    QObject * sl_ObjecQmlRequested(QString Path); /* return the obj2 from signal*/
public:
    SGuiQml();
    Q_INVOKABLE void m_connnecting(QString name ,QList<int> setting) {emit s_OperPort(name,setting);}

public slots:
    void sl_WindowQmlCreate();
    void sl_GetAlarm(QString str, Et::EventT type);
    void setStatusServer(bool stateser);
    Q_INVOKABLE void setSettingList (int i,int Data);
    Q_INVOKABLE void m_AddQuery(int type ,int startAdres ,int nubmer );

private slots:
    bool StatusServer();
signals:
    void StatusServerChanged();
    void s_SignalServer(int nPort);
    void s_OperPort(QString name,QList<int> settingsIndex);
    void s_DeleteController(int index);
    void s_StartStopQueryTimer(bool);



};

#endif // GUIQML_H
