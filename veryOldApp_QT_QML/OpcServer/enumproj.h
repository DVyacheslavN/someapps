#ifndef ENUMPROJ_H
#define ENUMPROJ_H
#include <QObject>
#include <QDataStream>
struct Et
{
    enum TcpT {
        query=1,
        AllIOModel,
        AImodel,
        DImodel,
        DOmodel,
        AOmodel,
        ErrorsList,
        ControllerList,
        itemsChange,
        intem_Change,
    };
    enum QueryT{
        GetAll=1,
        GetItem
    };
    enum EventT{
      Event=0,
        Alarm
    };
    enum TcpTypeList
    {
      A_AI=0,
        A_AO,
        A_DI,
        A_DO,
        A_Er,
        A_Controller,
        A_All,
        A_Item,
        A_Irems
    };
    enum TypeAllModel
    {
        AllDi=100,
        AllDo,
        AllAi,
        AllAo
    };
    template<typename T>
static    QByteArray m_TforByte(quint16 type, T data)
    {
        QByteArray byte;
        byte.clear();
        QDataStream goToTcp(&byte,QIODevice::WriteOnly);
        /* quint16(0) отвечает за размер блок чтобы принимающая сторона знала сколько
        всего данных придет  далее загоняем данные */
        quint16 xd=type;
        goToTcp << quint16(0) << xd << data;
        /*перейдем в начало нашего массива*/
        goToTcp.device()->seek(0);
        /*перезамишем начальную строчку quint16(0) размером данных
     *  при этом не будем учитывать сам quint16(0)*/
        goToTcp << quint16(byte.size() -sizeof(quint16));
        return byte;
    }
};
#endif // ET_H
