import QtQuick 2.0
import QtQuick.Controls 2.2
import "myjs.js" as Prog
Item {
    property var l_QueryObjectCreate
    signal on_StartStopQueryTimer (bool x)
    Rectangle {
    anchors.fill: parent
    color: "#1a1a1a"


    Button {
        id: o_AddQueryButton
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        width: 100
        height: 30
        text: "AddQuery"
        property var l_ObjCreate
        onClicked:
        {
            if (l_ObjCreate) l_ObjCreate.destroy()
            else{
                var comp=Qt.createComponent("AddQueryForm.qml")

                l_ObjCreate=comp.createObject(o_Root, {"id" : "o_AddQueryForm" ,
                                                  "z" :3 ,
                                                  "width" : 470});
                l_ObjCreate.anchors.horizontalCenter=o_Root.horizontalCenter
                l_ObjCreate.anchors.top= o_Root.top
                l_ObjCreate.anchors.topMargin= 100
            }
            // o_AddQueryForm.visible=!o_AddQueryForm.visible
            //            id: o_AddQueryForm
            //            anchors.horizontalCenter: parent.horizontalCenter
            //            anchors.top: parent.top
            //            anchors.topMargin: 100
            //            z:3
            //            width: 450

            //            visible: false
        }
    }


    Button {
        id: l_StartTimer
        anchors.right: o_AddQueryButton.left
        anchors.top: parent.top
        anchors.margins: 10
        width: 150
        height: 30
        text: "StartStopQueyuTimer"

        background: Rectangle
        {
            color: { if (parent.pressed ) return "#cccaca"

                else if (o_QueryList.QueryTimerStatus)return "green"
                else return "#7a7a7a"}
        }

        onClicked:
        {
            on_StartStopQueryTimer(!o_QueryList.QueryTimerStatus);
        }
    }
    Button{
        id: o_WriteFormCreate
        anchors{
            right: l_StartTimer.left
            top: parent.top
            margins: 10
        }
        width: 150
        height: 30
        property var l_ObjecCreate
        text: "WriteQuery"
        onClicked: {
            if (l_ObjecCreate) l_ObjecCreate.destroy()
            else{
                var comp=Qt.createComponent("WriteRegisterForm.qml")
                var obj;
                l_ObjecCreate=comp.createObject(o_Root, {"id" : "o_WriteRegisterForm" ,
                                                    "z" :3 ,
                                                    "width" : 604 ,
                                                    "height" : 64});
                l_ObjecCreate.anchors.horizontalCenter=o_Root.horizontalCenter
                l_ObjecCreate.anchors.top= o_Root.top
                l_ObjecCreate.anchors.topMargin= 100
            }
        }
    }
    Button {
        anchors{
            right: o_WriteFormCreate.left
            top: parent.top
            margins: 10

        }
        width: 100
        height: 30
        text:  "AnsverList"
        property var lA_ObjecCreate
        onClicked:
        {
            if (lA_ObjecCreate) lA_ObjecCreate.destroy()
            else{
                var comp=Qt.createComponent("AnswerList.qml")
                var obj;
                lA_ObjecCreate=comp.createObject(o_Root, {"id" : "o_AnswerList" ,
                                                    "z" :1 ,
                                                    "width" : 400 ,
                                                    "height" : 400});
                lA_ObjecCreate.anchors.centerIn=o_Root
//                lA_ObjecCreate.anchors.top= o_Root.top
//                lA_ObjecCreate.anchors.topMargin= 100
            }
        }
    }

    ListView{
        //interactive: false
        id: o_QueryView
        anchors.top: o_AddQueryButton.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        model: o_QueryList
        anchors.margins: 20
        property var l_QueryList: o_QueryList.m_QueryListGet();
        property bool l_selectItem: false

        delegate:  Rectangle {
            id: queryDelegate
            height: 20
            anchors.left: parent.left
            anchors.right:  parent.right
            property bool v_visibleList: false
            property var l_PortName: o_PortListModel.PortAdded[port]
            property var ql_settings: [type,port,conAdr,stAdr,numb]
            Row {
                id: l_rowDeleg
                spacing: 10
                height: 20
                // border.color: "black"
                width: parent.width
                Rectangle {
                    height: parent.height
                    width: height
                    visible:  numb
                    property bool v_logBit: queryDelegate.height == parent.height
                    color: v_logBit ? "green" : "red"
                    MouseArea{
                        anchors.fill: parent
                        onClicked:
                        {
                            if (l_QueryObjectCreate && parent.v_logBit)
                            {
                                l_QueryObjectCreate.parent.height=20
                                l_QueryObjectCreate.destroy()
                                o_QueryView.l_selectItem=false;

                            }

                            queryDelegate.v_visibleList = parent.v_logBit
                            if (parent.v_logBit && !o_QueryView.l_selectItem){
                                queryDelegate.height=(numb+1)*20
                                o_QueryView.l_selectItem=true;
                                l_QueryObjectCreate= Prog.createObject(index,queryDelegate)
                            }
                            else if (!parent.v_logBit) {
                                o_QueryView.l_selectItem=false;
                                queryDelegate.height=20
                                l_QueryObjectCreate.destroy()
                            }

                        }
                        onPressAndHold:{
                            if (!parent.v_logBit) {
                                o_QueryView.l_selectItem=false;
                                queryDelegate.height=20
                                l_QueryObjectCreate.destroy()
                            }
                            o_QueryList.m_DeleteQuery(index)
                        }
                    }
                }

                Text {
                    id: o_QueryType

                    height: 20
                    width: 150
                    // ["Coils","Discret Input","Input Registers","Holding Registers"]
                    text:   { "Type: " + o_QueryView.l_QueryList[type]
                        //var TypeText="Type:"
                        //                        switch (type)
                        //                        {

                        //                        case 1: return TypeText + "Coils";
                        //                        case 2: return TypeText +"Discret Input";
                        //                        case 3: return TypeText +"Input Registers";
                        //                        case 4: return TypeText +"Holding Registers";
                        //                        }
                    }

                }
                Text {
                    text: {
                        var textConTyep="TypeConnect:"
                        //                        switch (typeCon)
                        //                        {
                        //                        case 0: return textConTyep + "TCP"
                        //                        case 1: return textConTyep + "COM"
                        //                        }
                        return textConTyep + queryDelegate.l_PortName
                    }
                }
                Text {

                    text: "ControlAdr:"+conAdr
                }
                Text {
                    height: 20
                    //width: 50
                    text: "StartAddres:" + stAdr
                }
                Text {
                    height: 20
                    // width: 50
                    text: "Numbers:" + numb
                }
            }

            //            Row {

            //                visible:  false

            //            }
        }
    }
}
}
