#ifndef MODBUSDATACRONTROLLERMODEL_H
#define MODBUSDATACRONTROLLERMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QMetaEnum>
#include <QDebug>
class ModbusDataCrontrollerModel : public QAbstractListModel
{
    Q_OBJECT
public:
    //  array[regAdr, type, data, statusUpdata]
    ModbusDataCrontrollerModel();
    //array[regAdr, type, data, statusUpdata]
    QList<QList<int>> v_ModelData;
    QHash<int, QByteArray> roleNames()const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &) const override;
    void m_AddData(int regAdres, int regType, int data, int statusUpdates);
    inline bool m_Compare(int gPort, int gAddres, int regStart){ return (gAddres==v_addressContriller) &&
                (gPort==v_PortNmae) && (regStart==v_regStart) ;}
    enum my_roless{
        id=Qt::UserRole+1,
        regAdr,
        type,
        mydata,
        statusUpdate
    };
    Q_ENUM(my_roless)
    /*устанавливаетм данные по которым будет идти сравнение COMPARE*/
    Q_INVOKABLE void m_SetAddresAndPort(int gPort,int gAddres, int gregStart ,int gNumbReg)
    {v_addressContriller=gAddres; v_PortNmae=gPort; v_regStart=gregStart; v_numReg=gNumbReg; m_Clear();
        qDebug () <<gPort << "gPOrt" << gAddres <<"gAddres" << gregStart <<"GregStart" << gNumbReg <<"gNumbreg";}

    void m_SetAdresController(int addres) {v_addressContriller=addres;}

    Q_INVOKABLE void m_Delete(int index){ v_ModelData.removeAt(index); endResetModel();}
public slots:
    int m_GetPortId(){return v_PortNmae;}
    int m_GetAdressController() {return v_addressContriller;}
    int m_GetRegAddrs(int index) {return v_ModelData.at(index).at(0);}
    int m_GetRegData(int index) {return v_ModelData.at(index).at(2);}

private:
    void m_Clear () {v_ModelData.clear(); endResetModel();}
    int v_addressContriller;
    int v_PortNmae;
    int v_regStart;
    int v_numReg;
};

#endif // MODBUSDATACRONTROLLERMODEL_H
