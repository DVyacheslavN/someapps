#ifndef MODBUSDATAQUERYMODEL_H
#define MODBUSDATAQUERYMODEL_H
#include <QAbstractListModel>
#include <QModbusDataUnit>
#include <QMetaEnum>
#include <modbusdatacrontrollermodel.h>
typedef uint Ltype;
class ModbusDataQueryModel : public QAbstractListModel
{
    Q_OBJECT
    /*статус таймера опроса */
    Q_PROPERTY(bool QueryTimerStatus READ QueryTimerStatus WRITE setQueryTimerStatus NOTIFY QueryTimerStatusChanged)
public:
    ModbusDataQueryModel();
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QHash<int ,QByteArray> roleNames() const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    QList <Ltype*> v_DataUnitQuery;
    enum my_role{
        id=Qt::UserRole+1,
        type,
        port,
        typeCon,
        conAdr,
        stAdr,
        numb,
        status
    };
    Q_ENUM(my_role)
    QMetaEnum v_RoleEnum;
    QModbusDataUnit m_formatDataList(Ltype *data);
    Q_INVOKABLE void m_WriteRegisters (QVariant WriteRegData);
    //QStringList m_DataIndex(index);

public slots:
    void m_AddQuery(QVariant data );
    void m_FilterSet(int typeFilter);
    bool QueryTimerStatus() { return v_QueryTimerStatsu;}
    void setQueryTimerStatus(bool x) { v_QueryTimerStatsu=x; emit QueryTimerStatusChanged(); }
    QStringList m_QueryListGet(){ return v_NameRegTypeMod ;}
    void m_DeleteQuery (int index) { Ltype *buf=v_DataUnitQuery.at(index);
                                     v_DataUnitQuery.removeAt(index); delete [] buf; endResetModel(); emit s_QueryListChange(&v_DataUnitQuery);}
private:
    int *v_valueFilter;
    QList<int> m_filter(int row, int role) const;
    int v_max=0;
    int v_min=0;
    bool v_QueryTimerStatsu=false;
    char v_DataUnitQuerySize=5;
    /*тимы запросов modbus для GUI*/
    const   QStringList v_NameRegTypeMod  { "Invalid",
                                            "DiscreteInputs",
                                            "Coils",
                                            "InputRegisters",
                                            "HoldingRegisters"};
signals:
    void s_QueryListChange(QList<Ltype*> *QueryList);
    void QueryTimerStatusChanged();
    void s_WrieteRegister( QModbusDataUnit data, int typePort,int Addres);
};

struct LstQueryEnum
{
    enum {
        typeReg,
        PortIdConnect,
        addresClient,
        startS,
        numb,

    };
};

#endif // MODBUSDATAQUERYMODEL_H
