#ifndef SABSTRACTTCP_H
#define SABSTRACTTCP_H
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <enumproj.h>
class SAbstractTCP : public QObject
{
    Q_OBJECT
public:
    SAbstractTCP();
public slots:
    virtual void m_SignalServer(int nPort)=0;
private slots:
    virtual void m_NewConnect()=0;

signals:
    void s_EventTCP(QString,Et::EventT);
    void s_created();
private:

};

#endif // SABSTRACTTCP_H
