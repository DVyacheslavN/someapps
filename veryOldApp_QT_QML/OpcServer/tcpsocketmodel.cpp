#include "tcpsocketmodel.h"
#include <QHostAddress>
#include <QDebug>
TcpSocketModel::TcpSocketModel()
{
    m_enumToRoleName();
}

int TcpSocketModel::rowCount(const QModelIndex &) const
{
    return v_TcpList.size();
}

int TcpSocketModel::columnCount(const QModelIndex &) const
{
    return 1;// roleNames().size();
}

QVariant TcpSocketModel::data(const QModelIndex &index, int role) const
{

    if((my_comRole(Tcp_role::addres,role))) return v_TcpList.at(index.row())->peerAddress().toString();
    if(my_comRole(Tcp_role::status,role)) {
        return v_TcpList.at(index.row())->state();
    }
    return "def";
}

QHash<int, QByteArray> TcpSocketModel::roleNames() const
{
    QHash <int, QByteArray> roles ;//=QAbstractTableModel::roleNames();
    int x=0;
    for (int i=0 ; (x=v_roleEnum.value(i))>0 ;i++) {
        roles[x]=v_roleEnum.key(i);

    }
    return roles;
}

Qt::ItemFlags TcpSocketModel::flags(const QModelIndex &index) const
{
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

QModelIndex TcpSocketModel::index(int row, int column, const QModelIndex &) const
{
    return QAbstractTableModel::createIndex(row,column);
}

void TcpSocketModel::m_enumToRoleName()
{
    v_roleEnum = QMetaEnum::fromType<Tcp_role>();
}

bool TcpSocketModel::my_comRole(Tcp_role enumRole, int role) const
{
    return static_cast<int>(enumRole)== role;
    return true;
}

void TcpSocketModel::m_updateStatus()
{
//    if (v_TcpList.at(index.row())->state()==QTcpSocket::UnconnectedState)
//    {

//    }

     //v_TcpSocket,SLOT(deleteLater())
    auto buf =qobject_cast<QTcpSocket *> (sender());
    v_TcpList.removeOne(buf);
    buf->deleteLater();
    endResetModel();
}

void TcpSocketModel::m_DeleteClietn(int i)
{
    auto buf=v_TcpList.at(i);
    v_TcpList.removeAt(i);
    buf->deleteLater();
    endResetModel();

}

void TcpSocketModel::m_TcpSocketAppend(QTcpSocket *data)
{
    v_TcpList.append(data);
    connect(data,&QTcpSocket::stateChanged,this,&TcpSocketModel::endResetModel);
    connect(data,SIGNAL(disconnected()),this,SLOT(m_updateStatus()));
    endResetModel();

}
