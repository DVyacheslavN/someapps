#ifndef SABSTRACTEVENT_H
#define SABSTRACTEVENT_H
#include <QObject>
#include <enumproj.h>

class SabstractEvent : public QObject
{
    Q_OBJECT
public:
    SabstractEvent();
public slots:
     virtual void sl_addEvent (QString str,Et::EventT type)=0;
signals:
     void si_Alarm (QList<QString> str);
     void si_ShowEvent (QString str, Et::EventT);
};

#endif // SABSTRACTEVENT_H
