#ifndef TCPSOCKETMODEL_H
#define TCPSOCKETMODEL_H
#include <QAbstractTableModel>
#include <QTcpSocket>
#include <QMetaEnum>
class TcpSocketModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TcpSocketModel();
    virtual int rowCount(const QModelIndex &) const;
    virtual int columnCount(const QModelIndex &) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash <int , QByteArray> roleNames() const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual bool setData(const QModelIndex &, const QVariant &, int ) override {return true;}
    virtual QModelIndex index(int row, int column, const QModelIndex &parent=QModelIndex()) const;


    enum class Tcp_role {
        id=Qt::UserRole+1,
        addres,
        status

    };
    Q_ENUM(Tcp_role)

private:

    QMetaEnum v_roleEnum;
    QList<QTcpSocket*> v_TcpList;
    void m_enumToRoleName();
    bool my_comRole(Tcp_role enumRole, int role) const;
private slots:
    void m_updateStatus();
public slots:
    void m_DeleteClietn(int i);
    void m_TcpSocketAppend(QTcpSocket *data);

};

#endif // TCPSOCKETMODEL_H
